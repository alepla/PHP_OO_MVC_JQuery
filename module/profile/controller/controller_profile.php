<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/PHP_OO_MVC_JQuery/';
    include($path . "module/profile/model/DAOProfile.php");

    @session_start();
    
    switch($_GET['op']){

        case 'profile':
            include("module/profile/view/profile.php");
            break;

        case 'user':
            try{
                $daolawyer = new DAOProfile();
                $rdo = $daolawyer->select_user();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
            $lawyer = array();
                foreach ($rdo as $row){
                    array_push($lawyer, $row);
                }
                echo json_encode($lawyer);
                exit;
            }
            break;  

        case 'like_prods':
            try{
                $daolawyer = new DAOProfile();
                $rdo = $daolawyer->select_like_prod();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
            $lawyer = array();
                foreach ($rdo as $row){
                    array_push($lawyer, $row);
                }
                echo json_encode($lawyer);
                exit;
            }
            break;

        case 'delete_like_prods':

            try{
                $daolawyer = new DAOProfile();
                $rdo = $daolawyer->delete_prod($_GET['idd']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                echo json_encode($rdo);
                exit;
            }
            break;
            
        default:    
            include("view/inc/error404.php");
            break;
    }