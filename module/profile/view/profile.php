<div id="contenido">
    <div class="prof">
        <div class="card hovercard">
            <div class="card-background">
                <img class="card-bkimg" alt="" src="view/img/header.jpg"/>
            </div>
            <div class="useravatar">
                <img alt="" src="view/img/perfil.jpg"/>
            </div>
            <div class="card-info"> 
                <span class="card-title"></span>
            </div>
        </div>
        <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
            <div class="btn-group" role="group">
                <button type="button" id="stars" class="btn btn-default" href="#tab1" data-toggle="tab">
                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                    <p class="hidden-xs">Stars</p>
                </button>
            </div>
            <div class="btn-group" role="group">
                <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab">
                    <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                    <p class="hidden-xs">Info</p>
                </button>
            </div>
            <div class="btn-group" role="group">
                <button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    <p class="hidden-xs">Lawyers</p>
                </button>
            </div>
        </div>

            <div class="well">
          <div class="tab-content">
            <div class="tab-pane fade in active" id="tab1">
              <h3>My wish list</h3>
              <div class="prod_like"></div>
            </div>
            <div class="tab-pane fade in" id="tab2">
              <h3>My info</h3>
              <div class="my_info"></div>
            </div>
            <div class="tab-pane fade in" id="tab3">
              <h3>My Lawyers</h3>
              <div class="my_law"></div>
            </div>
          </div>
        </div>    
    </div>
</div>