<div class="about">
    <div class="container">
      
      <h2 class="w3l-titles" data-tr="Welcome">Welcome</h2>
      <div class="w3l-about-grids">
        <div class="col-md-6 w3ls-about-left">
          <div class="agileits-icon-grid">
            <div class="icon-left hvr-radial-out">
              <i class="fa fa-smile-o" aria-hidden="true"></i>
            </div>
            <div class="icon-right">
              <h5>100% SATISFIED CUSTOMERS</h5>
              <p>Phasellus dapibus felis elit, sed accumsan arcu gravida vitae. Nullam aliquam erat at lectus ullamcorper, nec interdum neque hendrerit.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
          <div class="agileits-icon-grid">
            <div class="icon-left hvr-radial-out">
              <i class="fa fa-usd" aria-hidden="true"></i>
            </div>
            <div class="icon-right">
              <h5>QUALITY SERVICE AFFORDABLE PRICE</h5>
              <p>Phasellus dapibus felis elit, sed accumsan arcu gravida vitae. Nullam aliquam erat at lectus ullamcorper, nec interdum neque hendrerit.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
          <div class="agileits-icon-grid">
            <div class="icon-left hvr-radial-out">
              <i class="fa fa-globe" aria-hidden="true"></i>
            </div>
            <div class="icon-right">
              <h5>WORLDWIDE LOCATIONS</h5>
              <p>Phasellus dapibus felis elit, sed accumsan arcu gravida vitae. Nullam aliquam erat at lectus ullamcorper, nec interdum neque hendrerit.</p>
            </div>
            <div class="clearfix"> </div>
          </div>
        </div>
        <div class="col-md-6 w3ls-about-right">
          <div class="w3ls-about-right-img">
            
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>


  <div class="clients">
  <div class="banner-dott1">
  <h3 class="w3l-titles" data-tr="Lawyers">Lawyers</h3>
    <div class="container">
      <h4> About Our Lawyers</h4>
      <section class="slider">
        <div class="flexslider">
          <ul class="slides">
            <li>
              <img src="view/img/Foto1.jpg" alt="" />
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
                <div class="client">
                  <h5>Harvey Specter</h5>
                </div>
            </li>
            <li>
              <img src="view/img/User3.jpg" alt="" />
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
                <div class="client">
                  <h5>Rachel Zane</h5>
                </div>
            </li>
            <li>
              <img src="view/img/Foto7.jpg" alt="" />
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
                <div class="client">
                  <h5>Luis Litt</h5>
                </div>
            </li>
            <li>
              <img src="view/img/c3.jpg" alt="" />
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation .</p>
                <div class="client">
                  <h5>Mike Ross</h5>
                </div>
            </li>
          </ul>
        </div>
      </section>
    </div>
  </div>
</div>

<div class="two-grids">
    <div class="container">
      <h3 class="w3l-titles" data-tr="Advantages">Advantages</h3>
      <p class="w3layouts_dummy_para"> In general, the types of cases our firm handles include:</p>
      <div class="col-md-6 w3_two_grid_right">
        <div class="w3_two_grid_right1">
          <div class="col-xs-3 w3_two_grid_right_grid">
            <div class="w3_two_grid_right_grid1">
              <i class="fa fa-hourglass-o" aria-hidden="true"></i>
            </div>
          </div>
          <div class="col-xs-9 w3_two_grid_right_gridr">
            <h4>Time</h4>
            <p>Suspendisse bibendum ex sit amet tellus finibus ultrices.</p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="w3_two_grid_right1">
          <div class="col-xs-3 w3_two_grid_right_grid">
            <div class="w3_two_grid_right_grid1">
              <i class="fa fa-clone" aria-hidden="true"></i>
            </div>
          </div>
          <div class="col-xs-9 w3_two_grid_right_gridr">
            <h4>works</h4>
            <p>Suspendisse bibendum ex sit amet tellus finibus ultrices.</p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="w3_two_grid_right1">
          <div class="col-xs-3 w3_two_grid_right_grid">
            <div class="w3_two_grid_right_grid1">
              <i class="fa fa-external-link" aria-hidden="true"></i>
            </div>
          </div>
          <div class="col-xs-9 w3_two_grid_right_gridr">
            <h4>CHANGE</h4>
            <p>Suspendisse bibendum ex sit amet tellus finibus ultrices.</p>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
      <div class="col-md-6 w3_two_grid_right">
        <div class="w3_two_grid_right1">
          <div class="col-xs-3 w3_two_grid_right_grid">
            <div class="w3_two_grid_right_grid1">
              <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
            </div>
          </div>
          <div class="col-xs-9 w3_two_grid_right_gridr">
            <h4>CONFIDETS</h4>
            <p>Suspendisse bibendum ex sit amet tellus finibus ultrices.</p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="w3_two_grid_right1">
          <div class="col-xs-3 w3_two_grid_right_grid">
            <div class="w3_two_grid_right_grid1">
              <i class="fa fa-check-square-o" aria-hidden="true"></i>
            </div>
          </div>
          <div class="col-xs-9 w3_two_grid_right_gridr">
            <h4>CONFIDENCE</h4>
            <p>Suspendisse bibendum ex sit amet tellus finibus ultrices.</p>
          </div>
          <div class="clearfix"> </div>
        </div>
        <div class="w3_two_grid_right1">
          <div class="col-xs-3 w3_two_grid_right_grid">
            <div class="w3_two_grid_right_grid1">
              <i class="fa fa-square-o" aria-hidden="true"></i>
            </div>
          </div>
          <div class="col-xs-9 w3_two_grid_right_gridr">
            <h4>100% OF OUR ENERGY</h4>
            <p>Suspendisse bibendum ex sit amet tellus finibus ultrices.</p>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>


<div class="services-bottom stats services">
  <div class="banner-dott1">
    <div class="container">
    
      <div class="wthree-agile-counter">
      <div class="col-md-3 w3_agile_stats_grid-top">
      <div class="w3_agile_stats_grid">
        <div class="agile_count_grid_left">
          <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
        </div>
        <div class="agile_count_grid_right">
          <p class="counter">324</p> 
        </div>
        <div class="clearfix"> </div>
        <h4>Win cases</h4>
      </div>
    </div>
    <div class="col-md-3 w3_agile_stats_grid-top">
      <div class="w3_agile_stats_grid">
        <div class="agile_count_grid_left">
          <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
        </div>
        <div class="agile_count_grid_right">
          <p class="counter">543</p> 
        </div>
        <div class="clearfix"> </div>
        <h4>Happy Clients</h4>
      </div>
    </div>
    <div class="col-md-3 w3_agile_stats_grid-top">
      <div class="w3_agile_stats_grid">
        <div class="agile_count_grid_left">
          <span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span>
        </div>
        <div class="agile_count_grid_right">
          <p class="counter">434</p> 
        </div>
        <div class="clearfix"> </div>
        <h4>People Loved</h4>
      </div>
    </div>
    <div class="col-md-3 w3_agile_stats_grid-top">
      <div class="w3_agile_stats_grid">
        <div class="agile_count_grid_left">
          <span class="fa fa-trophy" aria-hidden="true"></span>
        </div>
        <div class="agile_count_grid_right">
          <p class="counter">234</p> 
        </div>
        <div class="clearfix"> </div>
        <h4>Win Awards</h4>
      </div>
    </div>
      <div class="clearfix"> </div>
    </div>
  </div>
  </div>
</div>

  <div class="banner-bottom gallery">
    <div class="container">
    
    <div class="wthree-heading">
      <h2 class="w3l-titles" data-tr="Gallery">Gallery</h2>
      <p class="quia">Our Personal Connection with Clients</p>
    </div>
      <div class="w3ls_gallery_grids">
        <div class="col-md-4 w3_agile_gallery_grid">
          <div class="agile_gallery_grid">
            <a title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="view/img/g1.jpg">
              <div class="agile_gallery_grid1">
                <img src="view/img/g1.jpg" alt=" " class="img-responsive" />
                <div class="w3layouts_gallery_grid1_pos">
                  <h3>Law</h3>
                  <p> Justice</p>
                </div>
              </div>
            </a>
          </div>
          <div class="agile_gallery_grid">
            <a title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="view/img/g2.jpg">
              <div class="agile_gallery_grid1">
                <img src="view/img/g2.jpg" alt=" " class="img-responsive" />
                <div class="w3layouts_gallery_grid1_pos">
                  <h3>Law</h3>
                  <p> Justice</p>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 w3_agile_gallery_grid">
          <div class="agile_gallery_grid">
            <a title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="view/img/g3.jpg">
              <div class="agile_gallery_grid1">
                <img src="view/img/g3.jpg" alt=" " class="img-responsive" />
                <div class="w3layouts_gallery_grid1_pos">
                  <h3>Law</h3>
                  <p> Justice</p>
                </div>
              </div>
            </a>
          </div>
          <div class="agile_gallery_grid">
            <a title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="view/img/g4.jpg">
              <div class="agile_gallery_grid1">
                <img src="view/img/g4.jpg" alt=" " class="img-responsive" />
                <div class="w3layouts_gallery_grid1_pos">
                  <h3>Law</h3>
                  <p> Justice</p>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 w3_agile_gallery_grid">
          <div class="agile_gallery_grid">
            <a title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="view/img/g5.jpg">
              <div class="agile_gallery_grid1">
                <img src="view/img/g5.jpg" alt=" " class="img-responsive" />
                <div class="w3layouts_gallery_grid1_pos">
                  <h3>Law</h3>
                  <p> Justice</p>
                </div>
              </div>
            </a>
          </div>
          <div class="agile_gallery_grid">
            <a title="Donec sapien massa, placerat ac sodales ac, feugiat quis est." href="view/img/g6.jpg">
              <div class="agile_gallery_grid1">
                <img src="view/img/g6.jpg" alt=" " class="img-responsive" />
                <div class="w3layouts_gallery_grid1_pos">
                  <h3>Law</h3>
                  <p> Justice</p>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>