<div id="contenido">
	<section class="main">
		<section class="trabajos">
			<div class="contenedor">
				<div class="services">
					<div class="container">
						<div class="w3-agileits-heading">
							<h2 class="w3l-titles our">Our Services</h2> 
						</div>
					</div>	
				</div>
				<div class="laws">
					<?php
		                    if ($rdo->num_rows === 0){
		                        echo '<tr>';
		                        echo '<td align="center"  colspan="3">NO LAWYERS AVAILABLE</td>';
		                        echo '</tr>';
		                    }else{
		                        foreach ($rdo as $row) {
		                       		echo'
		                       		<div class="trabajo">
		                       			<div class="thumb" style="float:left">
		                       				<a style="cursor:pointer;" class=dumie id='.$row['dni'].'><img width=200 height=200 src="'. $row['image'] .'"/></a>
		                       				&nbsp;&nbsp;&nbsp;
		                       			<div class="descripcion">
						                  <p class="nombre">'. $row['name'] . " " . $row['lastname'] .'</p>
						                  <p class="categoria">'. $row['Salary'] . "$" .'</p>
					 		              <input type="number" id="'.$row['dni'].'qty" size="3" min="1" max="20" required value="1"/>
					 		              <button type="submit" class="products" id='.$row['dni'].'>Shop</button>					                  
						                </div>
		                       			</div>
		                       		</div>';    
		                       	}
		                    }
		                ?>
				</div>
			</div>
		</section>
	</section>

	<section id="lawyer_modal_dumie">
	    <div id="details_lawyer_dumie" hidden>
	        <div id="details">
	            <div id="container">
	                Name: <div id="name"></div></br>
	                LastName: <div id="lastname"></div></br>
	                Dni: <div id="dni"></div></br>
	                Telephone: <div id="tlp"></div></br>
	                Start Date: <div id="date0"></div></br>
	                End Date: <div id="date1"></div></br>
	                Gender: <div id="gender"></div></br>
	                Message: <div id="message"></div></br>
	                Type: <div id="type"></div></br>
	                City: <div id="city"></div></br>
	                Studies: <div id="studies"></div></br>
	                Salary: <div id="Salary"></div></br>
	            </div>
	        </div>
	    </div>
	</section>
</div>