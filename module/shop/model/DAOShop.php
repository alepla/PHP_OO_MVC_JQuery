<?php

    $path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/PHP_OO_MVC_JQuery/';
    include($path . "model/connect.php");
    
	class DAOShop{
		
		function select_all_dumies(){
            $sql = "SELECT * FROM dumies ORDER BY dni ASC";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }
		

        function select_dumie($dni){
            $sql = "SELECT * FROM dumies WHERE dni='$dni'";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
            return $res;
        }

        function select_all_studies(){
            $sql = "SELECT DISTINCT studies FROM dumies";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }

        function select_all_type($studies){
            $sql = "SELECT DISTINCT type FROM dumies WHERE studies='$studies'";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }

        function select_all_citys(){
            $tecla_pulsada = $_POST['service'];
            $sql = "SELECT * FROM dumies WHERE city LIKE '" . $tecla_pulsada . "%' GROUP BY id";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }

        function select_filter($studies,$type,$city){
            $sql = "SELECT DISTINCT * FROM dumies WHERE studies='$studies' AND type='$type' AND city='$city'";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }

        function filter($checks){
            $sql = "SELECT * FROM dumies $checks ORDER BY dni ASC";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }

    }