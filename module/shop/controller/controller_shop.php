<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/PHP_OO_MVC_JQuery/';
    include($path . "module/shop/model/DAOShop.php");

    @session_start();
    
    switch($_GET['op']){
    	case 'dumies':
            try{
                $daolawyer = new DAOShop();
                $rdo = $daolawyer->select_all_dumies();
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                include("module/shop/view/list_shop.php");
            }
            break;

        /*case 'list_dumies_jquery':
            try{
                $daolawyer = new DAOShop();
                $rdo = $daolawyer->select_all_dumies();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $lawyer = array();
                foreach ($rdo as $row){
                    array_push($lawyer, $row);
                }
                echo json_encode($lawyer);
                exit;
            }
            break;*/

        case 'read_modal_dumie':

            try{
                $daolawyer = new DAOShop();
                $rdo = $daolawyer->select_dumie($_GET['modal']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $lawyer=get_object_vars($rdo);
                echo json_encode($lawyer);
                exit;
            }
            break;

        case 'dropdown':

            try{
                $daolawyer = new DAOshop();
                $rdo = $daolawyer->select_all_studies();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $lawyer = array();
                foreach ($rdo as $row){
                    array_push($lawyer, $row);
                }
                echo json_encode($lawyer);
                exit;
            }
            break;

        case 'dropdown_type':

            try{
                $daolawyer = new DAOshop();
                $rdo = $daolawyer->select_all_type($_GET['uni']);
            }catch (Exception $e){
                echo json_encode("error1");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $lawyer = array();
                foreach ($rdo as $row){
                    array_push($lawyer, $row);
                }
                echo json_encode($lawyer);
                exit;
            }
            break;

        case 'list_filter':

            try{
                $daolawyer = new DAOshop();
                $rdo = $daolawyer->select_filter($_GET['uni'],$_GET['tipo'],$_GET['city']);
                
            }catch (Exception $e){
                echo json_encode("error1");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $lawyer = array();
                foreach ($rdo as $row){
                    array_push($lawyer, $row);
                }
                echo json_encode($lawyer);
                exit;
            }
            break;

        case 'redi':
            try{
                $daolawyer = new DAOShop();
                $rdo = $daolawyer->select_filter($_GET['uni'],$_GET['tipo'],$_GET['city']);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                include("module/shop/view/list.php");
            }
            break;

        case 'list_studies':
        //var_dump($_POST['service']);
            try{
                $daolawyer = new DAOShop();
                $rdo = $daolawyer->select_all_citys();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                foreach ($rdo as $row) {
                    echo '<div>
                            <a class="suggest-element" data="'.$row['city'].'" id="service'.$row['id'].'">'.utf8_encode($row['city']).'</a>
                        </div>';
                }
                exit;
            }
            break;

        case 'filter':
            try{
                $daolawyer = new DAOShop();
                $rdo = $daolawyer->filter($_GET['checks']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $lawyer = array();
                foreach ($rdo as $row){
                    array_push($lawyer, $row);
                }
                echo json_encode($lawyer);
                exit;
            }
        default:    
            include("view/inc/error404.php");
            break;
    }