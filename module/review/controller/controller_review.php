<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/PHP_OO_MVC_JQuery/';
    include($path . "module/review/model/DAOReview.php");

    //session_start();
    
    switch($_GET['op']){

        case 'list_reviews':

            include("module/review/model/validate.php");

            if ($_POST) {
                $result = validate_review();

                if ($result['resultado']) {
                    $_SESSION['review']=$_POST;
                    try{
                        $daolawyer = new DAOReview();
                        $rdo = $daolawyer->insert_review($_POST);
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }
                    
                    if($rdo){
                        echo '<script language="javascript">alert("Thaks for write a review.")</script>';
                        $callback = 'index.php?page=homepage';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }else{
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }
                }else{
                    $error = $result['error'];
                }
            }

            try{
                $daolawyer = new DAOReview();
                $rdo = $daolawyer->select_all_reviews();
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                include("module/review/view/list_review.php");
            }
            break;
            
        default:    
            include("view/inc/error404.php");
            break;
    }