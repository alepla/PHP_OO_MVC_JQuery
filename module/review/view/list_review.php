<div class="box">
    <h3 class="titulo_rev">Reviews</h3>
    <div class="box1">
        <?php
            if ($rdo->num_rows === 0){
                echo '<p> align="center"  colspan="3">NO REVIEWS YET</p>';
            }else{
                foreach ($rdo as $row) {
                    echo '<p class="user-name">'. $row['name'] . '<br></p>';
                    echo '<p class="user-rating">' . $row['estrellas'] .'<br></p>';
                    echo '<p class="user-review">'. $row['message'] .'<br></p>';
                    echo '<p class="user-time">'. $row['Day'] .'</p>';
                }
            }
        ?>
    </div>
    <div class="coment">
        <form class="review" method="post" name="review" id="review">
            <h3 class="na">WRITE A REVIEW</h3>

            <?php
                if(isset($error)){
                    print_r("<BR><span CLASS='styerror'>" . "* ".$error . "</span><br/>");
            }?>

            <input type="text" placeholder="Name" name="name" id="name" value="<?php echo $_POST?$_POST['name']:""; ?>">
            <span id="e_namere" class="styerror"></span>

            <input type="email" placeholder="Email" name="email" id="email" value="<?php echo $_POST?$_POST['email']:""; ?>">
            <span id="e_emailre" class="styerror"></span>

            <textarea type="text" name="message" placeholder="Review" id="message" value="<?php echo $_POST?$_POST['message']:""; ?>"></textarea>
            <span id="e_messagere" class="styerror"></span>
            <p class="Rating">Rating</p>
            <p class="clasificacion">
                <input id="radio1" type="radio" name="estrellas" value="★★★★★">
                <label for="radio1">★</label>
                <input id="radio2" type="radio" name="estrellas" value="★★★★">
                <label for="radio2">★</label>
                <input id="radio3" type="radio" name="estrellas" value="★★★">
                <label for="radio3">★</label>
                <input id="radio4" type="radio" name="estrellas" value="★★">
                <label for="radio4">★</label>
                <input id="radio5" type="radio" name="estrellas" value="★">
                <label for="radio5">★</label>
            </p>

          <input class="public" type="button" value="Public"  name="send" onclick="validate_review()">
        </form>
    </div>
</div>



