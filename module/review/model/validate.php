<?php
    function validate_review(){
        $error='';
        $filtro=array(
            'name' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/')
            ),
            'email' => array(
                'filter'=>FILTER_CALLBACK,
                'options'=>'validatemail'
            ),
            'message' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[a-zñáéíóúA-ZÁÉÍÓÚ0-9\s,.?¿!¡"&%$#@]{10,300}$/')
            )
        );

         $resultado=filter_input_array(INPUT_POST,$filtro); 
          if(!$resultado['name']){
            $error='Enter a valid name';
            }elseif(!$resultado['email'] || email_rep($resultado['email'])){
            $error='Enter a valid email, only one review for person';
            }elseif(!$resultado['message']){
               $error='The message must have between 10 and 300 characters'; 
           }else{
                return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
           };
           return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
    };

    function email_rep($email){
        $daolawyer = new DAOReview();
        $rdo = $daolawyer->select_review($email);
        return $rdo;
    }

    function validatemail($email){
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                if(filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp'=> '/^.{5,40}$/')))){
                    return $email;
                }
            }
            return false;
    }
?>