function validate_review(){

     if(document.review.name.value.length===0){
        document.getElementById('e_namere').innerHTML = "* You need to enter a name";
        document.review.name.focus();
        return 0;
    }
    document.getElementById('e_namere').innerHTML = "";

    if (document.review.email.value.length===0){
        document.getElementById('e_emailre').innerHTML = "* You need to enter a email";
        document.review.email.focus();
        return 0;
    }
    document.getElementById('e_emailre').innerHTML = "";

    if (document.review.message.value.length===0){
        document.getElementById('e_messagere').innerHTML = "* You need to enter a message";
        document.review.message.focus();
        return 0;
    }
    document.getElementById('e_messagere').innerHTML = "";

    document.review.submit();
    document.review.action="index.php?page=controller_review";
}