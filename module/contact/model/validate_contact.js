function validate_contact(){

     if(document.formcontact.name.value.length===0){
        document.getElementById('e_namecon').innerHTML = "* You need to enter a name";
        document.formcontact.name.focus();
        return 0;
    }
    document.getElementById('e_namecon').innerHTML = "";

    if (document.formcontact.email.value.length===0){
        document.getElementById('e_emailcon').innerHTML = "* You need to enter a email";
        document.formcontact.email.focus();
        return 0;
    }
    document.getElementById('e_emailcon').innerHTML = "";

    if (document.formcontact.message.value.length===0){
        document.getElementById('e_messagecon').innerHTML = "* You need to enter a message";
        document.formcontact.message.focus();
        return 0;
    }
    document.getElementById('e_messagecon').innerHTML = "";

    document.formcontact.submit();
    document.formcontact.action="index.php?page=controller_contact";
}

function initMap() {
        var uluru = {lat: 40.7127837, lng: -74.00594130000002};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });

        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h1 id="firstHeading" class="firstHeading">New York</h1>'+
            '<div id="bodyContent">'+
            '<p><b>New York City</b>, on the nature of the issue and each client’s unique needs, ' +
            'we can establish a fixed-fee budget, place a cap on fees, '+
            'utilize monthly retainers, or bill by the hour. In so doing, '+
            'our firm is compensated based on the quality of its services and mutually '+
            'beneficial client relationships.Our objective is to provide comprehensive, '+
            'efficient legal representation that is also cost effective. '+
            'At Pla Law Firm, we believe that our clients should '+
            'never be blindsided by unexpected legal fees. '+
            'In the event that a discrepancy arises, we are fully prepared to work out a solution. '+
            'Heritage Site.</p>'+
            '<p>Attribution: Uluru, '+
            '(last case december 19, 2018).</p>'+
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          title: 'New York (Manhattan)'
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
      }
