
	<section class="contact-w3ls">
	<div class="container">
		<div class="wthree-heading">
			<h2 class="w3l-titles">Contact Us</h2>
			<p class="quia">Just want to chat? It’s ok, send a question:</p>
		</div>
		<div class="con-top">
		<div class="col-lg-6 col-md-6 col-sm-6 contact-w3-agile2" data-aos="flip-left">
			<div class="contact-agileits">
				<h4>Get In Touch</h4>
				<form class="formulario" method="post" name="formcontact" id="formcontact">
		        	<?php
			        if(isset($error)){
			            prin_r ("<BR><span CLASS='styerror'>" . "* ".$error . "</span><br/>");
			        }?>
			        <div class="control-group form-group">
	                    <div class="controls">
					        <label class="contact-p1">Name:</label> 
				            <input type="text" class="form-control" name="name" id="name" value="<?php echo $_POST?$_POST['name']:""; ?>">
				            <span id="e_namecon" class="styerror"></span>
				            <p class="help-block"></p>
				        </div>
	                </div>
	                <div class="control-group form-group">
	                    <div class="controls">
		            <label class="contact-p1">Email Address:</label>
		            <input type="email" class="form-control" name="email" id="email" value="<?php echo $_POST?$_POST['email']:""; ?>">
		            <span id="e_emailcon" class="styerror"></span>
		            <p class="help-block"></p>
		            	</div>
		            </div>

		            <div class="control-group form-group">
	                    <div class="controls">
		            <label class="contact-p1">Message:</label>
		            <textarea type="text" class="form-control" name="message" id="message" value="<?php echo $_POST?$_POST['message']:""; ?>"></textarea>
		            <span id="e_messagecon" class="styerror"></span>
		            <p class="help-block"></p>
		            	</div>
		            </div>
		            <input class="btn btn-primary" type="button" value="Send"  name="send" onclick="validate_contact()">
		        </form>            
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 contact-w3-agile1" data-aos="flip-right">
			<h4 class="w3l-contact">Connect With Us</h4>
			<p class="contact-agile1"><strong>Phone :</strong> +1 (100)685-631-152</p>
			<p class="contact-agile1"><strong>Email :</strong> <a href="mailto:name@example.com">info@example.com</a></p>
			<p class="contact-agile1"><strong>Address :</strong> 132 New Lenox, 868 1st Avenue,4th United States.</p>											
			
							<ul class="agileits_social_list">
								<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
							</ul>
						
		</div>
		<div class="clearfix"></div>
		</div>
	</div>
</section>
<!--API INFO-->
<div class="api">
  <h1 class="wiki-api-title">Don't understand any term? <br/> Don't worry search it!</h1>
  <input id="searchItem" name="search">
  <button id='search' type="button" class ="btn-api">Search</button>
  <div id="show"></div>
</div>
<!--END APPI INFO-->
 
<div class="w3l-map">
	<div id="map" style="height: 400px;"></div>
</div>