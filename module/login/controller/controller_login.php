<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/PHP_OO_MVC_JQuery/';
    include($path . "module/login/model/DAOLogin.php");
    //include("module/login/model/validate.php");

    @session_start();
    
    switch($_GET['op']){

        case 'list_login':
        
            include("module/login/view/login.php");
            break;

        case 'login':

            if ($_POST){
                try{
                    $daolawyer = new DAOLogin();
                    $rdo = $daolawyer->select_user($_POST['username'], $_POST['passwd']);
                    $_SESSION['user'] = $_POST['username'];
                }catch (Exception $e){
                    echo "Error";
                    exit;
                }
                if(!$rdo){
                    echo "Error name user or password dosen't exists";
                    exit;
                }else{
                    $pass = get_object_vars($rdo);
                    if(password_verify($_POST['passwd'],$pass['passwd'])){
                        echo "ok";
                        foreach ($rdo as $row){
                            $_SESSION['type'] = $row;
                        }
                        exit;
                    }
                    else{
                        echo "Error name user or password dosen't exists";
                        exit;
                    }
                }
            }
            break;

        case 'register_login':

            if ($_POST){
                try{
                    $daolawyer = new DAOLogin();
                    $rdo = $daolawyer->create_user($_POST);
                }catch (Exception $e){
                    echo "Error";
                    exit;
                }
                if(!$rdo){
                    echo "This user name or email alrready exists";
                    exit;
                }else{
                    echo "good";
                    exit;
                }
            }
            break;

        case 'list_register_login':

            include("module/login/view/register_login.php");    
            break;

        case 'logout':
        
            unset($_SESSION['type']);
    
            if(session_destroy()){

                echo '<script>window.location="index.php?page=homepage";</script>';

            }
            break;

        case 'recover_passwd':
            
            if ($_POST){
                try{
                    $daolawyer = new DAOLogin();
                    $rdo = $daolawyer->update_passwd($_POST);
                }catch (Exception $e){
                    echo "Error";
                    exit;
                }
                if($rdo){
                    try{
                        $daolawyer = new DAOLogin();
                        $rdo = $daolawyer->select_update($_POST['email']);
                        }catch (Exception $e){
                            echo "Error";
                            exit;
                        }
                        if(!$rdo){
                            echo "Error, this email doesn't exists";
                            exit;
                        }else{
                            echo "good"; 
                            exit;
                    }
                }
            }
            break;
            
        case 'list_recover':

            include("module/login/view/recover_passwd.php");
            break;

            /*case 'register_login':

                if ($_POST) {
                    $result = validate_login_register();

                    if ($result['resultado']) {
                        $_SESSION['user']=$_POST;
                        try{
                            $daolawyer = new DAOLogin();
                            $rdo = $daolawyer->create_user($_POST);
                        }catch (Exception $e){
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                        
                        if($rdo){
                            echo '<script language="javascript">alert("The user was created correctly.")</script>';
                            $callback = 'index.php?page=homepage';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }else{
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                    }else{
                        $error = $result['error'];
                    }
                }

                try{
                    $daolawyer = new DAOLogin();
                    $rdo = $daolawyer->select_all_users();
                }catch (Exception $e){
                    $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                }
                
                if(!$rdo){
                    $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                }else{
                    include("module/login/view/register_login.php");
                }
                break;*/
            
        default:  

            include("view/inc/error404.php");
            
            break;
    }