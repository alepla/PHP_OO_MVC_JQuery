function validate_login(){

     if(document.login.username.value.length===0){
        document.getElementById('e_username').innerHTML = "* You need to enter a user name";
        document.login.username.focus();
        return 0;
    }
    document.getElementById('e_username').innerHTML = "";

    if (document.login.passwd.value.length===0){
        document.getElementById('e_passwd').innerHTML = "* You need to enter a password";
        document.login.passwd.focus();
        return 0;
    }
    document.getElementById('e_passwd').innerHTML = "";

    document.login.click();
    document.login.action="index.php?page=controller_login";
}

$(document).ready(function(){ 

    $("#login").submit(function(e){
        e.preventDefault();

        if(validate_login() != 0){

            var data = $("#login").serialize();
            console.log(data);
                
            $.ajax({
                    
                type : 'POST',
                url  : 'module/login/controller/controller_login.php?op=login&' + data ,
                data : data,
                beforeSend: function(){  

                    $("#error").fadeOut();
                },
                success :  function(response){
                console.log(response);

                    if(response=="ok"){              
                        setTimeout(' window.location.href = "index.php?page=homepage"; ',2000);
                    }else{
                                    
                        $("#error").fadeIn(1000, function(){                        
                            $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
                        });
                    }
                }
            });
        }
    });
});