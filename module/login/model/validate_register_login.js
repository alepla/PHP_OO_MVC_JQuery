function validate_register_login(){

    if(document.login_register.name.value.length===0){
        document.getElementById('e_name').innerHTML = "* You need to enter a user name";
        document.login_register.name.focus();
        return 0;
    }
    document.getElementById('e_name').innerHTML = "";

    if(document.login_register.lastname.value.length===0){
        document.getElementById('e_lastname').innerHTML = "* You need to enter a user name";
        document.login_register.lastname.focus();
        return 0;
    }
    document.getElementById('e_lastname').innerHTML = "";

     if(document.login_register.username.value.length===0){
        document.getElementById('e_username').innerHTML = "* You need to enter a user name";
        document.login_register.username.focus();
        return 0;
    }
    document.getElementById('e_username').innerHTML = "";

    if (document.login_register.email.value.length===0){
        document.getElementById('e_email').innerHTML = "* You need to enter a email";
        document.login_register.email.focus();
        return 0;
    }
    document.getElementById('e_email').innerHTML = "";

    if (document.login_register.passwd.value.length===0){
        document.getElementById('e_paswd').innerHTML = "* You need to enter a password";
        document.login_register.passwd.focus();
        return 0;
    }
    document.getElementById('e_paswd').innerHTML = "";

    if (document.login_register.passwdrep.value.length===0){
        document.getElementById('e_paswd2').innerHTML = "* You need to enter a password";
        document.login_register.passwdrep.focus();
        return 0;
    }
    document.getElementById('e_paswd2').innerHTML = "";

    passwd1 = document.login_register.passwd.value;
    passwd2 = document.login_register.passwdrep.value;

    if(passwd1 != passwd2){
        document.getElementById('e_paswd').innerHTML = "* The password must be the same";
        document.login_register.passwd.focus();
        return 0;
    }
    document.getElementById('e_paswd').innerHTML = "";

    document.login_register.click();
    document.login_register.action="index.php?page=controller_login";
}

$(document).ready(function(){ 

    $("#login_register").submit(function(e){
        e.preventDefault();
        
        if(validate_register_login() != 0){

            var data = $("#login_register").serialize();
            console.log(data);
                
            $.ajax({
                    
                type : 'POST',
                url  : 'module/login/controller/controller_login.php?op=register_login&' + data ,
                data : data,
                beforeSend: function(){  

                    $("#error").fadeOut();
                },
                success :  function(response){
                console.log(response);

                    if(response=="good"){              
                        setTimeout(' window.location.href = "index.php?page=controller_login&op=list_login"; ',2000);
                    }else{
                                    
                        $("#error").fadeIn(1000, function(){                        
                            $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
                        });
                    }
                }
            });
        }
    });
});
