<?php

    $path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/PHP_OO_MVC_JQuery/';
    include($path . "model/connect.php");
    
	class DAOLogin{

		function create_user($user){
            $hashed_password = crypt($user[passwd], '$l$rasmusles$');
            $sql = "INSERT INTO login(name, lastname, username, email, passwd, type) "
                . " VALUES ('$user[name]', '$user[lastname]', '$user[username]', '$user[email]', '$hashed_password', '2')";
            
            $conexion = Connect::con();
            $res = mysqli_query($conexion, $sql);
            Connect::close($conexion);
            return $res;
        }

        function select_all_users(){
            $sql = "SELECT * FROM login ORDER BY name ASC";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }

        function select_user($username){
            $sql = "SELECT * FROM login WHERE username='$username'";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
            return $res;
        }

        function update_passwd($passwd){
            $hashed_passwd = crypt($passwd[passwd], '$l$rasmusles$');
            $sql = "UPDATE login SET passwd='$hashed_passwd' WHERE email='$passwd[email]'";

            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }

        function select_update($email){
            $sql = "SELECT * FROM login WHERE email='$email'";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
            return $res;
        }

    }