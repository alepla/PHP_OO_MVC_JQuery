function validate_recover_passwd(){

    if (document.recover.email.value.length===0){
        document.getElementById('e_email').innerHTML = "* You need to enter a email";
        document.recover.email.focus();
        return 0;
    }
    document.getElementById('e_email').innerHTML = "";

    if (document.recover.passwd.value.length===0){
        document.getElementById('e_passwd').innerHTML = "* You need to enter a password";
        document.recover.passwd.focus();
        return 0;
    }

    document.recover.click();
    document.recover.action="index.php?page=controller_login";
}

$(document).ready(function(){ 

    $("#recover").submit(function(e){
        e.preventDefault();

        if(validate_recover_passwd() != 0){

            var data = $("#recover").serialize();
            console.log(data);
                
            $.ajax({
                    
                type : 'POST',
                url  : 'module/login/controller/controller_login.php?op=recover_passwd&' + data ,
                data : data,
                beforeSend: function(){  

                    $("#error").fadeOut();
                },
                success :  function(response){
                console.log(response);

                    if(response=="good"){              
                        setTimeout(' window.location.href = "index.php?page=controller_login&op=list_login"; ',2000);
                    }else{
                                    
                        $("#error").fadeIn(1000, function(){                        
                            $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
                        });
                    }
                }
            });
        }
    });
});