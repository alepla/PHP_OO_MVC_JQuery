<div id="contenido">
    <div class="contenido_login">
    <div class="register_form">
        <h1 class="title_log">Register</h1>
        <form method="post" name="login_register" id="login_register" class="form-signin">

            <div id="error"></div>

            <p>
                <label for="name">Name</label>
                <input name="name" id="name" type="text" class="form-control" value="<?php echo $_POST?$_POST['name']:""; ?>" />
                <span id="e_name" class="styerror"></span>
            </p>
            <p>
                <label for="lastname">Last Name</label>
                <input name="lastname" id="lastname" type="text" class="form-control" value="<?php echo $_POST?$_POST['lastname']:""; ?>" />
                <span id="e_lastname" class="styerror"></span>
            </p>
            <p>
                <label for="username">User Name</label>
                <input name="username" id="username" type="text" class="form-control" value="<?php echo $_POST?$_POST['username']:""; ?>" />
                <span id="e_username" class="styerror"></span>
            </p>
            <p>
                <label for="email">Email</label>
                <input name="email" id="email" type="email" class="form-control" value="<?php echo $_POST?$_POST['email']:""; ?>" />
                <span id="e_email" class="styerror"></span>
            </p>
            <p>
                <label for="passwd">Password</label>
                <input name="passwd" id="passwd" type="password" class="form-control" value="<?php echo $_POST?$_POST['passwd']:""; ?>" />
                <span id="e_paswd" class="styerror"></span>
            </p>
            <p>
                <label for="passwdrep">Repit the Password</label>
                <input name="passwdrep" id="passwdrep" type="password" class="form-control" value="<?php echo $_POST?$_POST['passwdrep']:""; ?>" />
                <span id="e_paswd2" class="styerror"></span>
            </p>
            <input name="Submit" type="submit" class="btn btn-lg btn-primary btn-block btn-signin" value="Register" onclick="validate_register_login()"/>
        </form>
    </div>
    <div class="why_cont">
        <h3 class="why_title">Why you should register</h3>
        <i class="fa fa-check-square-o" aria-hidden="true"></i><p class="why_p">·Follow other users with similar cases who can advise you</p>
        <p class="why_p">·Find the lawyer you need</p>
        <p class="why_p">·Give your opinion about lawyers</p>
        <p class="why_p">·Enjoy special promotions</p>
        <p class="why_p">·There are only two clicks!</p>
    </div>
    </div>
</div>