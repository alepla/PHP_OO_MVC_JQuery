<div id="contenido">
    <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="view/img/perfil.jpg" />
        <form method="post" name="login" id="login" class="form-signin">

            <div id="error"></div>

            <p>
                <label for="username">User Name</label>
                <input name="username" id="username" type="text" placeholder="User Name" class="form-control" value="<?php echo $_POST?$_POST['username']:""; ?>" />
                <span id="e_username" class="styerror"></span>
            </p>
            <p>
                <label for="passwd">Password</label>
                <input name="passwd" id="passwd" type="password" placeholder="Password" class="form-control" value="<?php echo $_POST?$_POST['passwd']:""; ?>" />
                <span id="e_passwd" class="styerror"></span>
            </p>
            <input name="Submit" type="submit" class="btn btn-lg btn-primary btn-block btn-signin" value="Sign In" id="btn-login" onclick="validate_login()"/>
            <a href="index.php?page=controller_login&op=list_recover" class="register">Forgot youre passwrod?</a>
            </br>
            <a href="index.php?page=controller_login&op=list_register_login" class="register">Register</a>
        </form>
    </div> 
</div>