<div id="contenido">
    <div class="card card-container">
        <form method="post" name="recover" id="recover" class="form-signin">
            <div id="error"></div>
            <p>
                <label for="email">Email</label>
                <input name="email" id="email" type="email" placeholder="Email" class="form-control" value="<?php echo $_POST?$_POST['email']: ""; ?>" />
                <span id="e_email" class="styerror"></span>
            </p>
            <p>
                <label for="passwd">New Password</label>
                <input name="passwd" id="passwd" type="password" placeholder="Password" class="form-control" value="<?php echo $_POST?$_POST['passwd']: ""; ?>" />
                <span id="e_passwd" class="styerror"></span>
            </p>
            <input name="Submit" type="submit" class="btn btn-lg btn-primary btn-block btn-signin" value="Send" id="btn-recover" onclick="validate_recover_passwd()"/>
        </form>
    </div> 
</div>