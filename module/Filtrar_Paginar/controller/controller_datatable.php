<?php

    $path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/PHP_OO_MVC_JQuery/';
    include($path . "module/Filtrar_Paginar/model/DAOdatatable.php");

    //session_start();
    
    switch($_GET['op']){
    	 case 'list':
            try{
                $daolawyer = new DAODatatable();
            	$rdo = $daolawyer->select_all_lawyer();
            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
    			$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
                include("module/Filtrar_Paginar/view/list_datatable.php");
    		}
            break;

    	case "datatable":
    		try{
                $daolawyer = new DAODatatable();
                $rdo = $daolawyer->select_all_lawyer();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
            	$lawyer = array();
                foreach ($rdo as $row){
                	array_push($lawyer, $row);
                }
                echo json_encode($lawyer);
                exit;
            }
            break;

    	default:    
            include("view/inc/error404.php");
            break;
    }