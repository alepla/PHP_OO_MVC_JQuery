
$(document).ready(function () {
    var url = "module/Filtrar_Paginar/controller/controller_datatable.php?op=datatable";
    // prepare the data
    var source =
    {
        dataType: "json",
        dataFields: [
            { name: 'name', type: 'string' },
            { name: 'lastname', type: 'string' },
            { name: 'dni', type: 'string' },
            { name: 'tlp', type: 'string' }
        ],
        id: 'id',
        url: url
    };
    var dataAdapter = new $.jqx.dataAdapter(source);
    console.log(source);
    $("#dataTable").jqxDataTable(
        {
            width: getWidth("dataTable"),
            pageable: true,
            pagerButtonsCount: 10,
            source: dataAdapter,
            columnsResize: true,
            columns: [
                { text: 'Name', dataField: 'name', width: 220 },
                { text: 'Lastname', dataField: 'lastname', width: 220 },
                { text: 'DNI', dataField: 'dni', width: 210 },
                { text: 'Telephone', dataField: 'tlp', width: 200 }
            ]
        });
});