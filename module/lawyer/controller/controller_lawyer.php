<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/PHP_OO_MVC_JQuery/';
    include($path . "module/lawyer/model/DAOLawyer.php");

    //session_start();
    
    switch($_GET['op']){
        case 'list':
            try{
                $daolawyer = new DAOLawyer();
            	$rdo = $daolawyer->select_all_lawyer();
            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
    			$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
                include("module/lawyer/view/list_lawyer.php");
    		}
            break;

        case 'create':

        include("module/lawyer/model/validate.php");

            if ($_POST) {
                $result = validate_lawyer();

                if ($result['resultado']) {
                    $_SESSION['lawyer']=$_POST;
                    try{
                        $daolawyer = new DAOLawyer();
    		            $rdo = $daolawyer->insert_lawyer($_POST);
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
        			    die('<script>window.location.href="'.$callback .'";</script>');
                    }
                    
		            if($rdo){
            			echo '<script language="javascript">alert("You are registred in the DataBase correctly")</script>';
            			$callback = 'index.php?page=controller_lawyer&op=list';
        			    die('<script>window.location.href="'.$callback .'";</script>');
            		}else{
            			$callback = 'index.php?page=503';
    			        die('<script>window.location.href="'.$callback .'";</script>');
            		}
                }else{
                    $error = $result['error'];
                }
            }
        
            include("module/lawyer/view/create_lawyer.php");
            break;

        case 'update':

            include("module/lawyer/model/validate.php");
            
            if ($_POST) {
                $result = validate_lawyer_dni();

                if ($result['resultado']) {
                    $_SESSION['lawyer']=$_POST;
                    try{
                        $daolawyer = new DAOLawyer();
                        $rdo = $daolawyer->update_lawyer($_POST);
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }
                    
                    if($rdo){
                        echo '<script language="javascript">alert("The lawyers was updated correctly")</script>';
                        $callback = 'index.php?page=controller_lawyer&op=list';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }else{
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }
                }else{
                    $error = $result['error'];
                }
            }
            
            try{
                $daolawyer = new DAOLawyer();
                $rdo = $daolawyer->select_lawyer($_GET['id']);  
                $lawyer=get_object_vars($rdo);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }

            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                include("module/lawyer/view/update_lawyer.php");
            }
            break;

        case 'delete':
            if (isset($_POST['delete'])){
                try{
                    $daolawyer = new DAOLawyer();
                    $rdo = $daolawyer->delete_lawyer($_GET['id']);
                }catch (Exception $e){
                    $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                }
                
                if($rdo){
                    echo '<script language="javascript">alert("The lawyer was deleted")</script>';
                    $callback = 'index.php?page=controller_lawyer&op=list';
                    die('<script>window.location.href="'.$callback .'";</script>');
                }else{
                    $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                }
            }
            try{
                $daolawyer = new DAOLawyer();
                $rdo = $daolawyer->select_lawyer($_GET['id']);
                $lawyer=get_object_vars($rdo);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }else{
                include("module/lawyer/view/delete_lawyer.php");
            }
            break;

        case 'read_modal':

            try{
                $daolawyer = new DAOLawyer();
                $rdo = $daolawyer->select_lawyer($_GET['modal']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $lawyer=get_object_vars($rdo);
                echo json_encode($lawyer);
                exit;
            }
            break;
            
        default:    
            include("view/inc/error404.php");
            break;
    }