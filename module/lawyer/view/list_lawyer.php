<div id="contenido" >
    <div class="crimes">
        <h1 class="new_clients">VIEW NEW CLIENTS</h1>
        <input class="search_crime">
        <button type="button" class="search_crim">SEARCH</button>
        <div id="show_crime"></div>
    </div>
    <div class="container">
        <div class="crud">
    	<div class="row">
    			<h3 class="title-law" data-tr="Lawyers list">Lawyers list</h3>
    	</div>
    	<div class="row">
    		<p><a class="Button_create" data-tr="create" href="index.php?page=controller_lawyer&op=create">Create</a></p>
    		
    		<table class="table-law">
                <tr class="law">
                    <td width=125><b>DNI</b></td>
                    <td width=125 data-tr="Name"><b>Name</b></td>
                    <td witdth=125  data-tr="Lastname"><b>Last Name</b></td>
                    <td width=350  data-tr="Actions"><b>Actions</b></td>
                </tr>
                <?php
                    if ($rdo->num_rows === 0){
                        echo '<tr>';
                        echo '<td align="center"  colspan="3">NO USERS AVAILABLE</td>';
                        echo '</tr>';
                    }else{
                        foreach ($rdo as $row) {
                       		echo '<tr>';
                    	   	echo '<td width=125>'. $row['dni'] . '</td>';
                    	   	echo '<td width=125>'. $row['name'] . '</td>';
                    	   	echo '<td width=125>'. $row['lastname'] . '</td>';
                    	   	echo '<td width=350>';
                            echo '<a class="lawyer Button_blue" id='.$row['dni'].' data-tr="read">Read</a>';
                    	   	echo '&nbsp;';
                    	   	echo '<a class="Button_green" href="index.php?page=controller_lawyer&op=update&id='.$row['dni'].'" data-tr="update">Update</a>';
                    	   	echo '&nbsp;';
                    	   	echo '<a class="Button_red" href="index.php?page=controller_lawyer&op=delete&id='.$row['dni'].'" data-tr="delete">Delete</a>';
                    	   	echo '</td>';
                    	   	echo '</tr>';
                        }
                    }
                ?>
            </table>
    	</div>
        </div>
    </div>
</div>

<section id="lawyer_modal">
    <div id="details_lawyer" hidden>
        <div id="details">
            <div id="container">
                Name: <div id="name"></div></br>
                LastName: <div id="lastname"></div></br>
                Dni: <div id="dni"></div></br>
                Telephone: <div id="tlp"></div></br>
                Start Date: <div id="date0"></div></br>
                End Date: <div id="date1"></div></br>
                Gender: <div id="gender"></div></br>
                Message: <div id="message"></div></br>
                Studies: <div id="studies"></div></br>
                Salary: <div id="Salary"></div></br>
            </div>
        </div>
    </div>
</section>