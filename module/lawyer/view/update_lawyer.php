<div id="contenido">
    <div class="cont">
    <form method="post" name="formlawyers" id="formlawyers" class="contacte">
        <?php
        if(isset($error)){
            print_r("<BR><span CLASS='styerror'>" . "* ".$error . "</span><br/>");
        }?>
        <p>
            <label for="name" data-tr="Name">Name</label>
            <input name="name" id="name" type="text" placeholder="Name" value="<?php echo $lawyer['name']; ?>" />
            <span id="e_name" class="styerror"></span>
        </p>
        <p>
            <label for="lastname" data-tr="Last Name">Last Name</label>
            <input name="lastname" id="lastname" type="text" placeholder="Latname" value="<?php echo $lawyer['lastname']; ?>" />
            <span id="e_lastname" class="styerror"></span>
        </p>
        <p>
            <label for="dni">DNI</label>
            <input name="dni" id="dni" type="text" placeholder="DNI" value="<?php echo $lawyer['dni']; ?>" readonly/>
            <span id="e_dni" class="styerror"></span>
        </p>
        <p>
            <label for="tlp" data-tr="Telephone">Telephone</label>
            <input name="tlp" id="tlp" type="text" placeholder="Telephone" value="<?php echo $lawyer['tlp']; ?>" />
            <span id="e_tlp" class="styerror"></span>
        </p>
        <p>
            <label for="date0" data-tr="Start Date">Start Date</label>
            <input name="date0" id="date0" type="text" placeholder="Start Date" value="<?php echo $lawyer['date0']; ?>" />
            <span id="e_date" class="styerror"></span>
        </p>
        <p>
            <label for="date1" data-tr="End Date">End Date</label>
            <input name="date1" id="date1" type="text"  placeholder="End Date" value="<?php echo $lawyer['date1']; ?>" />
            <span id="e_date1" class="styerror"></span>
        </p>
        <p>
            <label for="gender" data-tr="Gender">Gender</label>
            <select id="gender" name="gender" class="gender">
                    <?php
                        if($lawyer['gender']==="Male"){
                    ?>
                        <option value="Male" selected>Male</option>
                        <option value="Female">Female</option>
                    <?php
                        }else{
                    ?>
                        <option value="Male">Male</option>
                        <option value="Female" selected>Female</option>
                    <?php
                        }
                    ?>
            </select>
            <span id="e_gender" class="styerror"></span>
        </p>
        <p>
            <label for="message" data-tr="Message">Message</label>
            <textarea name="message" id="message" type="text"><?php echo $lawyer['message'];?></textarea>
            <span id="e_message" class="styerror"></span>
        </p>
        <p>
            <label for="studies" data-tr="Studies">Studies:</label>
            <?php
                $studi=explode(" ", $lawyer['studies']);
            ?>
            <label>
                <?php
                    $busca_array=in_array("Hardvard", $studi);
                    if($busca_array){
                ?>
                Hardvard<input type="checkbox" id="studies[]" name="studies[]" class="studies" value="Hardvard" checked/>
                <?php
                    }else{
                ?>
                Hardvard<input type="checkbox" id="studies[]" name="studies[]" class="studies" value="Hardvard"/>
                <?php
                    }
                ?>
                <?php
                    $busca_array=in_array("Oxford", $studi);
                    if($busca_array){
                ?>
                Oxford<input type="checkbox" id="studies[]" name="studies[]" class="studies" value="Oxford" checked/>
                <?php
                    }else{
                ?>
                Oxford<input type="checkbox" id="studies[]" name="studies[]" class="studies" value="Oxford"/>
                <?php
                        }
                ?>
                <?php
                    $busca_array=in_array("Columbia", $studi);
                    if($busca_array){
                ?>
                Columbia<input type="checkbox" id="studies[]" name="studies[]" class="studies" value="Columbia" checked/>
                <?php
                    }else{
                ?>
                Columbia<input type="checkbox" id="studies[]" name="studies[]" class="studies" value="Columbia"/>
                <?php
                    }
                ?>
                <?php
                    $busca_array=in_array("Other", $studi);
                    if($busca_array){
                ?>
                Other<input type="checkbox" id="studies[]" name="studies[]" class="studies" value="Other" checked/>
                <?php
                    }else{
                ?>
                Other<input type="checkbox" id="studies[]" name="studies[]" class="studies" value="Other"/>
                <?php
                    }
                ?>
            </label>
            <span id="e_studies" class="styerror"></span>
        </p>
        <p>
            <label for="price" data-tr="Salary:">Salary:</label>
            <label>
                    <?php
                        if ($lawyer['Salary']==="1000"){
                    ?>
                        1000 <input name="Salary" type="radio" class="salary" value="1000" checked>
                        2000 <input name="Salary" type="radio" class="salary" value="2000">
                        3000 <input name="Salary" type="radio" class="salary" value="3000">
                        4000 <input name="Salary" type="radio" class="salary" value="4000">
                    <?php
                        }elseif ($lawyer['Salary']==="2000"){
                    ?>
                        1000 <input name="Salary" type="radio" class="salary" value="1000">
                        2000 <input name="Salary" type="radio" class="salary" value="2000" checked>
                        3000 <input name="Salary" type="radio" class="salary" value="3000">
                        4000 <input name="Salary" type="radio" class="salary" value="4000">
                    <?php
                        }elseif ($lawyer['Salary']==="3000"){
                    ?>
                        1000 <input name="Salary" type="radio" class="salary" value="1000">
                        2000 <input name="Salary" type="radio" class="salary" value="2000">
                        3000 <input name="Salary" type="radio" class="salary" value="3000" checked>
                        4000 <input name="Salary" type="radio" class="salary" value="4000">
                    <?php    
                        }else{
                    ?>
                        1000 <input name="Salary" type="radio" class="salary" value="1000">
                        2000 <input name="Salary" type="radio" class="salary" value="2000">
                        3000 <input name="Salary" type="radio" class="salary" value="3000">
                        4000 <input name="Salary" type="radio" class="salary" value="4000" checked>
                    <?php   
                        }
                    ?>
            </label>
            <span id="e_studies" class="styerror"></span>
        </p>
        <input name="Submit" type="button" class="button" value="Update" onclick="validate_lawyer()" />
        <a class="back" href="index.php?page=controller_lawyer&op=list" data-tr="Back">Back</a>
    </form>
    </div>
</div>