$(document).ready(function () {
        $('.lawyer').click(function () {
            var id = this.getAttribute('id');

            $.get("module/lawyer/controller/controller_lawyer.php?op=read_modal&modal=" + id, function (data, status) {
                var json = JSON.parse(data);
                console.log(json);
                
                if(json === 'error') {
                    window.location.href='index.php?page=503';
                }else{
                    console.log(json.dni);
                    $("#name").html(json.name);
                    $("#lastname").html(json.lastname);
                    $("#dni").html(json.dni);
                    $("#tlp").html(json.tlp);
                    $("#date0").html(json.date0);
                    $("#date1").html(json.date1);
                    $("#gender").html(json.gender);
                    $("#message").html(json.message);
                    $("#studies").html(json.studies);
                    $("#Salary").html(json.Salary);
         
                    $("#details_lawyer").show();
                    $("#lawyer_modal").dialog({
                        width: 500,
                        height: 500, //<!--  ------------- altura de la ventana -->
                        //show: "scale", <!-- ----------- animación de la ventana al aparecer -->
                        //hide: "scale", <!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        //position: "down",<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "toggle",
                            duration: 1000
                        },
                        hide: {
                            effect: "fadeToggle",
                            duration: 1000
                        }
                    });
                }//end-else
            });
        });
        $(".search_crim").on("click", function() {

            var crime = $(".search_crime").val();

            $.get( "http://nflarrest.com/api/v1/crime/arrests/" + crime + "&limit=12", function( data ) {

              $("#show_crime").empty();
              $( "#show_crime" ).html();

                for(var i = 0; i < data.length; i++){
                  
                  $( "#show_crime" ).prepend("<div class='appi2'><div class='api1'>NAME: " + data[i].Name + "<br/>DATE: "+ data[i].Date + "<br/><br/>DESCRIPTION: " + data[i].Description + "</div></div>");
                }
            });
        });
    });