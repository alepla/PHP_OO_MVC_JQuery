<?php

    $path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/PHP_OO_MVC_JQuery/';
    include($path . "model/connect.php");
    
	class DAOLawyer{

        function insert_lawyer($lawyer){
            $university = '';
            foreach ($lawyer[studies] as $studie) {
                $university = $university. $studie. " ";
            }

             $sql = "INSERT INTO info(name, lastname, dni, tlp, gender, date0, date1, message, studies, Salary,"
                . " Hour, Day) VALUES ('$lawyer[name]', '$lawyer[lastname]', '$lawyer[dni]', '$lawyer[tlp]', '$lawyer[gender]',"
                . " '$lawyer[date0]', '$lawyer[date1]', '$lawyer[message]', '$university','$lawyer[Salary]', now(), now())";
            
            $conexion = Connect::con();
            $res = mysqli_query($conexion, $sql);
            Connect::close($conexion);
            return $res;
        }

        function update_lawyer($lawyer){
            $univerity ='';
            foreach ($lawyer[studies] as $studie) {
                $university = $university. $studie. " ";
            }

            $sql = "UPDATE info SET name='$lawyer[name]', lastname='$lawyer[lastname]', tlp='$lawyer[tlp]', gender='$lawyer[gender]', date0='$lawyer[date0]',"
                . " date1='$lawyer[date1]', message='$lawyer[message]', studies='$university', Salary='$lawyer[Salary]' WHERE dni='$lawyer[dni]'";

            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }

        function delete_lawyer($dni){
            $sql = "DELETE FROM info WHERE dni='$dni'";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }
		
		function select_all_lawyer(){
			$sql = "SELECT * FROM info ORDER BY dni ASC";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
		}

        function select_lawyer($dni){
            $sql = "SELECT * FROM info WHERE dni='$dni'";
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
            return $res;
        }
    }