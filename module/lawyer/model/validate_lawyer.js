function validate_studies(){
    var ok = 0;
    var ckbox = document.getElementsByName('studies[]');
    for(i = 0; i < ckbox.length; i++){
        if(ckbox[i].checked == true){
            ok = 1;
            return true;
        }
    }
    if(ok == 0){
        return false;
    }
}


function validate_lawyer() {
    if (document.formlawyers.name.value.length===0){
        document.getElementById('e_name').innerHTML = "* You need to enter a name";
        document.formlawyers.name.focus();
        return 0;
    }
    document.getElementById('e_name').innerHTML = "";
    
    if (document.formlawyers.lastname.value.length===0){
        document.getElementById('e_lastname').innerHTML = "* You need to enter a lastname";
        document.formlawyers.lastname.focus();
        return 0;
    }
    document.getElementById('e_lastname').innerHTML = "";
    
    if (document.formlawyers.dni.value.length===0){
        document.getElementById('e_dni').innerHTML = "* You need to enter a DNI";
        document.formlawyers.dni.focus();
        return 0;
    }
    document.getElementById('e_dni').innerHTML = "";
    
    if (document.formlawyers.tlp.value.length===0){
        document.getElementById('e_tlp').innerHTML = "* You need to enter a Telephone";
        document.formlawyers.tlp.focus();
        return 0;
    }
    document.getElementById('e_tlp').innerHTML = "";
    
    if (document.formlawyers.date0.value.length===0){
        document.getElementById('e_date').innerHTML = "* You need to enter a date";
        document.formlawyers.date0.focus();
        return 0;
    }
    document.getElementById('e_date').innerHTML = "";
    
    if (document.formlawyers.date1.value.length===0){
        document.getElementById('e_date1').innerHTML = "* You need to enter a second date";
        document.formlawyers.date1.focus();
        return 0;
    }
    document.getElementById('e_date1').innerHTML = "";

    if (document.formlawyers.message.value.length===0){
        document.getElementById('e_message').innerHTML = "* You need to enter a message";
        document.formlawyers.message.focus();
        return 0;
    }
    document.getElementById('e_message').innerHTML = "";

    var v_studies=document.getElementsByName('studies[]');
    var r_studies=validate_studies(v_studies);
    if(!r_studies){
        document.getElementById('e_studies').innerHTML = "* At least you need to check one universtiy";
        return 0;
    }
    else{
        document.getElementById('e_studies').innerHTML = "";
    }

    document.formlawyers.submit();
    document.formlawyers.action="index.php?page=controller_lawyer";
}



