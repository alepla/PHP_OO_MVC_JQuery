<?php

/*Validate-php-lawyer----------------------------------------------------------------------------*/

    function validate_lawyer(){
        $error='';
        $filtro = array(
            'name' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/')
            ),
            
            'lastname' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/')
            ),
            
            'dni' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^([0-9]{8}[A-Z]{1})$/')
            ),
            
            'tlp' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^[0-9]{9}$/')
            ),
            
            'message' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[a-zñáéíóúA-ZÁÉÍÓÚ0-9\s,.?¿!¡"&%$#@]{10,300}$/')
            )
        );
        
        $resultado=filter_input_array(INPUT_POST,$filtro); 
        if(!$resultado['name']){
            $error='Enter a valid name';
        }elseif(!$resultado['lastname']){
            $error='Enter a valid lastname';
        }elseif(!$resultado['dni'] || id_rep($resultado['dni'] )){
            $error='Enter a valid DNI';
        }elseif(!$resultado['tlp']){
            $error='Enter a valid telephone';
        }elseif(!$resultado['message']){
            $error='The message must have between 10 and 300 characters';    
        }else{
             return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
        };
        return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
    };

/*End-Validate-php-lawyer----------------------------------------------------------------------------*/

/*Validate-php-lawyer-dni_rep----------------------------------------------------------------------------*/
    
    function id_rep($dni){
        $daolawyer = new DAOLawyer();
        $rdo = $daolawyer->select_lawyer($dni);
        return $rdo;
    }

/*End-Validate-php-lawyer-dni_rep----------------------------------------------------------------------------*/

/*Validate-php-lawyer_update----------------------------------------------------------------------------*/
    
    function validate_lawyer_dni(){
        $error='';
        $filtro = array(
            'name' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/')
            ),
            
            'lastname' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/')
            ),
            
            'dni' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^([0-9]{8}[A-Z]{1})$/')
            ),
            
            'tlp' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^[0-9]{9}$/')
            ),
            
            'message' => array(
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => array('regexp' => '/^[a-zñáéíóúA-ZÁÉÍÓÚ0-9\s,.?¿!¡"&%$#@]{10,300}$/')
            )
        );
        
        $resultado=filter_input_array(INPUT_POST,$filtro); 
        if(!$resultado['name']){
            $error='Enter a valid name';
        }elseif(!$resultado['lastname']){
            $error='Enter a valid lastname';
        }elseif(!$resultado['tlp']){
            $error='Enter a valid telephone';
        }elseif(!$resultado['message']){
            $error='The message must have between 10 and 300 characters';    
        }else{
             return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
        };
        return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
    };

/*End-Validate-php-lawyer_update----------------------------------------------------------------------------*/

?>
