  <div class="main-textgrids">
    <div class="container">
      <div class="w3ls-heading">
        <h2 class="w3l-titles">About Us</h2>
        <p class="w3layouts_dummy_para">WE PUT OUR CLIENTS FIRST</p>
      </div>
      <div class="ab-agile">
        <div class="col-md-5 ab-pic">
           <img src="view/img/balance.jpg" alt=" " />
        </div>
        <div class="col-md-7 ab-text">
          <p>Pla's Law Firm, we rely on state-of-the-art technology to communicate with clients, including email, text messaging, Skype, Facebook, Twitter, GoTo Webinars, and the like.As an Pla's Law Firm client, you can expect to benefit from the latest technology. Our clients benefit from having all their legal documents available to them at the touch of their fingertips through our app that is available on both IPhone and Android devices. This enables us to reach out to clients in a unique, convenient and secure manner. </p>
          <ul class="ab">
            <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-angle-right" aria-hidden="true"></i> BA, Sociology, University of Minnesota, Twin Cities (2001)</a></li>
            <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-angle-right" aria-hidden="true"></i> JD, Mitchell Hamline School of Law, Magna Cum Laude (2007)</a></li>
            <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-angle-right" aria-hidden="true"></i> MA, Nonprofit Management, Hamline University (2010)</a></li>
            <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-angle-right" aria-hidden="true"></i> Planning Commissioner, City of Mounds View (2014 – 2015)</a></li>
          </ul>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="statements">
        <div class="col-md-7 mission">
           <h4>FEE PHILOSOPHY</h4>
           <p>Unlike most law firms that bill clients based on the total billable hours each lawyer works on a particular matter, Pla's Law Firm, P.C. employs a variety of alternative fee structures. Depending on the nature of the issue and each client’s unique needs, we can establish a fixed-fee budget, place a cap on fees, utilize monthly retainers, or bill by the hour. In so doing, our firm is compensated based on the quality of its services and mutually beneficial client relationships.Our objective is to provide comprehensive, efficient legal representation that is also cost effective. At Pla's Law Firm, we believe that our clients should never be blindsided by unexpected legal fees. In the event that a discrepancy arises, we are fully prepared to work out a solution.</p>
          <ul class="ab">
            <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-angle-right" aria-hidden="true"></i> Economic Development Commissioner (2014 – 2015)</a></li>
            <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-angle-right" aria-hidden="true"></i> Want more? Check out my LinkedIn profile.</a></li>
            <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-angle-right" aria-hidden="true"></i> Buildings and Construction;</a></li>
            <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-angle-right" aria-hidden="true"></i> Energy (including renewables, utilities and power);</a></li>
          </ul>
        </div>
        <div class="col-md-5 facts">
          <img src="view/img/award.jpg" alt=" " />   
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
  <div class="different">
    <div class="container">
      
      <div class="w3l-heading">
        <h3 class="w3l-titles">Why We are Different</h3>
      </div>
      
      <div class="w3agile-different-info">
        <p>When other methods of settlement fail and you are owed money or services you can’t collect, a civil lawsuit may be your only alternative. What gives us a powerful advantage in the courtroom is that we always go to court fully prepared, having performed a thorough investigation of the background of both sides of the argument, gathered all necessary related documents, and deposed any and all key witnesses. Beyond that, we keep in close contact with the opposing attorneys in order to keep the option of settlement open right up until the moment that we go before the judge..</p>
        <div class="w3agile-button">
          <a href="#" data-toggle="modal" data-target="#myModal">Read More</a>
        </div>
      </div>
    </div>
  </div>
  <div class="team">
    <div class="container">
      <div class="w3ls-heading">
        <h3 class="w3l-titles">Team</h3>
        <p class="w3layouts_dummy_para">Belleville Trial Lawyers</p>
      </div>
      <div class="agileinfo-team-grids">
        <div class="col-md-3 wthree-team-grid">
          <img src="view/img/Foto7.jpg" alt="">
          <div class="wthree-team-grid-info">
            <h4>Luis Litt</h4>
            <p>Dence Lawyer</p>
            <div class="team-social-grids">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 wthree-team-grid">
          <img src="view/img/Foto1.jpg" alt="">
          <div class="wthree-team-grid-info">
            <h4>Harvey Specter</h4>
            <p>Criminal Lawyer</p>
            <div class="team-social-grids">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 wthree-team-grid">
          <img src="view/img/User3.jpg" alt="">
          <div class="wthree-team-grid-info">
            <h4>Rachel Zane</h4>
            <p>Public Defender</p>
            <div class="team-social-grids">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 wthree-team-grid">
          <img src="view/img/c3.jpg" alt="">
          <div class="wthree-team-grid-info">
            <h4>Mike Ross</h4>
            <p>Defence Lawyer</p>
            <div class="team-social-grids">
              <ul>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>