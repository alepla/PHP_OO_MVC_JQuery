<h2 class="title_cesta">Cart</h2>
<div class="title_cart"></div>
<div id="cart_hiden">
	<div class="alert alert-info">
		<span class="glyphicon glyphicon-asterisk"></span><p style="padding-left: 15px;">No one product in you're cart!</p>
	</div>
</div>
<div id="complete_shop"></div>
<div id="fact_law"></div>
<div id="div_factura"></div>
<div id="cart" border="1" style="visibility:hidden; width:100%">
  <div id="cartBody"></div>
  <div id="total"></div>
</div>