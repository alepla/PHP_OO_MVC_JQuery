<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/PHP_OO_MVC_JQuery/';
    include($path . "module/cart/model/DAOCart.php");

    @session_start();
    
    switch($_GET['op']){
    	case 'cart':
            include("module/cart/view/cart.php");
            break;

        case 'add':

            try{
                $daolawyer = new DAOCart();
                $rdo = $daolawyer->select_product($_GET['prod']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $lawyer=get_object_vars($rdo);
                echo json_encode($lawyer);
                exit;
            }
            break;

        case 'shop':
            if ((isset($_SESSION['type']))){
                $lawyer=get_object_vars($rdo);
                echo json_encode($lawyer);
                exit;
            }else{
                echo json_encode("error");
                exit;
            }
            break;
            
        case 'insert':

            try{
                $daolawyer = new DAOCart();
                $rdo = $daolawyer->insert_shop($_SESSION['user'], $_GET['dni_user'], $_GET['total_ind'], $_GET['quantity']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                echo json_encode($rdo);
                exit;
            }
            break;

        case 'fact':
            try{
                $daolawyer = new DAOCart();
                $rdo = $daolawyer->select_user();
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
            $lawyer = array();
                foreach ($rdo as $row){
                    array_push($lawyer, $row);
                }
                echo json_encode($lawyer);
                exit;
            }
            break; 

        case 'like':

            try{
                $daolawyer = new DAOCart();
                $rdo = $daolawyer->insert_like_prods($_SESSION['user'], $_GET['name_like'], $_GET['lastname_like'], $_GET['dni_like'], $_GET['image_like'], $_GET['studies_like'], $_GET['salary_like']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                echo json_encode($rdo);
                exit;
            }
            break;

        default:    
            include("view/inc/error404.php");
            break;
    }