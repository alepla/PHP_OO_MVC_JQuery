var cart = [];
var total = 0;
var total_qty = 0;
$(function () {
    if (localStorage.cart){
        cart = JSON.parse(localStorage.cart);
        showCart();
        $("#num_prod").html(cart.length);
    }
});
$(document).ready(function(){
    $(document).on('click','.products_like', function () {
        var id_like = this.getAttribute('id');
        console.log(id_like)
        $.post("module/cart/controller/controller_cart.php?&op=add&prod=" + id_like, function (data, status) {
            var json = JSON.parse(data);
            console.log(json);
            if(json === 'error') {
                window.location.href='index.php?page=503';
            }else{
                console.log(json.name);
                console.log(json.lastname);
                console.log(json.dni);
                console.log(json.image);
                console.log(json.studies);
                console.log(json.Salary);
                $.get("module/cart/controller/controller_cart.php?op=like&name_like=" + json.name + "&lastname_like=" + json.lastname + "&dni_like=" + json.dni + "&image_like=" + json.image + "&studies_like=" + json.studies + "&salary_like=" + json.Salary, function (data, status) {
                        var json = JSON.parse(data);
                        if(json === 'error') {
                            alert("Sorry, some error has ocurred")
                        }else{
                            alert("Lawyer added to wish list");
                        }
                });
            }
        });
    });
    $(document).on('click','.products', function () {
        var id = this.getAttribute('id');
        $.post("module/cart/controller/controller_cart.php?&op=add&prod=" + id, function (data, status) {
            var json = JSON.parse(data);
            console.log(json);
            if(json === 'error') {
                window.location.href='index.php?page=503';
            }else{
                var price = json.Salary;
                var name = json.name + " " + json.lastname;
                var image = json.image;
                var studies = json.studies;
                var dni = json.dni;
                var qty = $("#"+ id + "qty").val();
                // update qty if product is already present
                for (var i in cart) {
                    if(cart[i].Product == name){
                        cart[i].Qty = qty;
                        showCart();
                        saveCart();
                        return;
                    }
                }
                // create JavaScript Object
                var item = { Product: name,  Price: price, Qty: qty, Img: image, Studies: studies, DNI: dni}; 
                cart.push(item);
                saveCart();
                //showCart();
                $("#num_prod").html(cart.length);
            }
        });
    });
    $(".qtyy").change(function(){
        qty = $(this).val();
        console.log(qty);
        for (var i in cart) {
            var q = $(this).attr('id')
            if(cart[i].DNI == q.substr(0, q.length-3)){
                cart[i].Qty = qty;
                saveCart();
                cargarDiv("#cartBody","index.php?page=controller_cart&op=cart");
                return;
            }
        }
    });
    $('#btn_shop').click(function (){
        $.post("module/cart/controller/controller_cart.php?&op=shop", function (data, status) {
            var json = JSON.parse(data);
            //console.log(json);
            if(json === 'error') {
                window.location.href = "index.php?page=controller_login&op=list_login";
            }else{
                for (var i in cart) {
                    var dni_user = cart[i].DNI;
                    var quantity = cart[i].Qty;
                    var total_ind = cart[i].Qty * cart[i].Price;
                    console.log(dni_user);
                    console.log(cart[i].Qty);
                    console.log(cart[i].Qty * cart[i].Price);
                    $.get("module/cart/controller/controller_cart.php?op=insert&dni_user=" + dni_user + "&total_ind=" + total_ind + "&quantity=" + quantity, function (data, status) {
                        var json = JSON.parse(data);
                        if(json === 'error') {
                            $("#complete_shop").append('<div class="alert alert-danger"><span class="glyphicon glyphicon-remove"></span>Sorry, something has gone wrong!</div>');
                        }else{
                            localStorage.removeItem('cart');
                            $("#cartBody").empty();
                            $("#total").empty();
                            $("#complete_shop").append('<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> Succes, this lawyer is now under you´re services!</div>');
                        }
                    });
                }
                $("#div_factura").append('<button id="btn_factura" type="button">Invoice</button>');
                $('#btn_factura').click(function (){
                    $("#complete_shop").empty();
                    $("#div_factura").empty();
                    $.post("module/cart/controller/controller_cart.php?&op=fact", function (data, status) {
                        var jso = JSON.parse(data);
                        var manolo = jso[0];
                        var top = "<div class='div_fact'>" +
                                    "<h2 class='fact_rigth'>INVOICE</h2>" +
                                    "<div class='fact_left'>" +
                                    "<h2>Pla's Lawyers</h2>" +
                                    "<h2>Shop information</h2>" +
                                    "<p>"+ manolo.username + "</p>" +
                                    "<p>"+ manolo.name + manolo.lastname + "</p>" +
                                    "<p>"+ manolo.email + "</p>" +
                                    "</div>" +
                                  "</div>";

                        $("#fact_law").append(top);
                        for (var j in cart) {
                        var ro = "<div class='fact_prod'>" +
                                      "<div class='w3_two_grid_left1 cesta'>" +
                                        "<div class='col-xs-3 w3_two_grid_left_grid img_cesta'><img src=" + cart[j].Img + "></div>" +
                                        "<h4 class='name_prod'>" + cart[j].Product + "</h4>" +
                                        "<h4 class='studies_prod'>" + cart[j].Studies + "</h4>" +
                                        "<a class='fas'>$" + cart[j].Qty * cart[j].Price + ",00</a>" +
                                        "<a class='fass'>" + cart[j].Qty + "</a>" +
                                        "<div class='clearfix'></div>" +
                                      "</div>" +
                                  "</div>";

                        $("#fact_law").append(ro);
                        }
                        var tota = "<div class='fact_tt'><p class='fact_tpt'>Subtotal (" + total_qty + " lawyers):</p></br>" +
                                    "<p class='fact_tot'>$" + total + ",00</p></div>" +
                                    "<button id='btn_factura_back' class='btn_factura_bac' type='button'>Back</button>";

                        $("#fact_law").append(tota);
                        $('#btn_factura_back').click(function (){
                            window.location.href='index.php?page=controller_profile&op=profile';
                        });
                    });
                });
            }
        });
    });
});

function cargarDiv(div,url) {
    $(div).load(url);
}

function deleteItem(index){
    cart.splice(index,1); // delete item at index
    total = 0;
    total_qty = 0;
    showCart();
    saveCart();
    cargarDiv("#cartBody","index.php?page=controller_cart&op=cart");
    $("#num_prod").html(cart.length);
}

function saveCart() {
    if ( window.localStorage){
        localStorage.cart = JSON.stringify(cart);
    }
}

function showCart() {
    if (cart.length == 0) {
        $("#cart").css("visibility", "hidden");
        return;
    }
    $("#cart_hiden").empty();
    $("#cart").css("visibility", "visible");
    $("#cartBody").empty();
    for (var i in cart) {
        var item = cart[i];
        total = parseFloat(total) + (parseFloat(item.Qty) * parseFloat(item.Price));
        total_qty = parseFloat(total_qty) + parseFloat(item.Qty);
        var row = "<div class='w3_two_grid_left1 cesta'>" +
                    "<div class='col-xs-3 w3_two_grid_left_grid img_cesta'><img src=" + item.Img + "></div>" +
                    "<div class='col-xs-3 w3_two_grid_right_grid'>" +
                    "<h4 class='name_prod'>" + item.Product + "</h4>" +
                    "<h4 class='studies_prod'>" + item.Studies + "</h4>" +
                    "<p class='stock'>In stock</p>" +
                    "<a class='delete_prod' onclick='deleteItem(" + i + ")'>Delete</a>" +
                    "</div><div><a class='prod_price'>$" + item.Qty * item.Price + ",00</a>" +
                    "<select id='"+item.DNI+"qty' class='qtyy'>" +
                        "<option>" + item.Qty + "</option>" +
                        "<option value='1'>1</option>" +
                        "<option value='2'>2</option>" +
                        "<option value='3'>3</option>" +
                        "<option value='4'>4</option>" +
                        "<option value='5'>5</option>" +
                    "</select>" +
                    "<div class='clearfix'></div>" +
                  "</div>";
        $("#cartBody").append(row);
    }
    var shop = "<div class='panel_shop'>" +
                "<div><p class='panel_qty'>Subtotal (" + total_qty + " lawyers):</p></div>" +
                "<div><p class='panel_price'>$" + total + ",00</p></div>" +
                "<input type='submit' class='btn_panel_shop' value='Proced to checkout' id='btn_shop'></input>" +
                "<div><p class='o_panel'>or</p></div>" +
                "<div><a class='log_panel' href='index.php?page=controller_login&op=list_login'>Sign In</a></div>" +
               "</div>";
    $("#total").empty();
    $("#total").append(shop);

}