Apart de tindre tot el que has demanat a clase, com lo del dni repetit o ampliar la informació que se mostra a l'hora de esborrar un usuari...

En el temps lliure he fet algo més com:

1.-He creat una secció de reviews on els usuaris poden escriure la seva critica i valorar amb estrelles, una volta enviada la valoració es guarda en una BD, i al mateix temps es mostra la valoració al costat del formulari on escriure la valoració. Es a dir en el controller envie la valoració i la pinte al mateix temps. Amb el css fet a ma personalment.

2.-En el contact us vaig crear un mini formulari en el cual els usuaris poden enviar les seves preguntes.

3.-També he fet que la informació que es filtra per el dropdown i el autocomplete siga pintada en una pagina apart però també tinc la forma en la que es pinta per el js, mitjansan el jason_encode.

4.-I del que estic més orgullos pero no se si es pot considerar una millora, ja que era obligat anyadir-ho. El autocomplete que no funcionava bé per les funsions, ja siga per les llibreries, o la verció... Bueno pos la mateixa semana que ho vas manar em vag posar a traureu però em vag donar conter que no em traia la informació que jo volia, o al fer click no funcionava, o no desapareixien les sugerencies, i una a una vag anar revisant les funcions i vag acabar canviat 3 o 4, però seguia sense funcionar. Desesperat perque ja no sabia que passava vag preguntar en stackoverflow: 

https://es.stackoverflow.com/questions/131626/autocompletar-campo-con-jquery-ajax-y-php

On em van ajudar dienteme que canviara el var dataString = 'service='+service; per var dataString = {service: service}; que era el principal problema. 
Escric tot açò més que res perque em vag tirar els meus dies arreglant-ho tot, jo sols i després este és el codic que s'ha passat per tota la clase, sols han copiat i pegat, que m'alegre per ells, millor, pero per a que no cregues que el meu codic es una altra copia més, sinós tot el contrai, el original. Podrás veure que la pregunta de Stackoverflow està al meu nom. Serà una tonteria pero em va dur les meues hores i crec que ningú més de la clase ho a tret sense acò.

5.-Un recover password, on es demana el email del usuari i la nova contrasenya i per ajax envia la nova informació al DAO el qual fa un update de la contrasenya antiga.

6.-Encripte la contraseña al clavarla a base de dades.

7.-Tinc tres apis, una en el contact, que búsca en wikipedia lo que escrius en un input, altra en el CRUD que busca a jugadors de la NFL per els seus crimens, i altre en el shop on pots buscar el contingut de la teua cartera de eth i veure el preu actual de la moneda eth.

8.-Un profile on depenent de que usuari siga es mostra el seu nom + els adbocats que tens comprats + informasió personal.

9.-Un filtro en el shop on depenent de que selecciones es mostren uns adbocats o altres.

10.-Cart(Comprar), es adir tu pots comprar els usuaris i es guarden en la BD.

11.-I crec que "la millora" de validació de JS/PHP es la meua forma de validar els formularis.

12.-Per últim, per estudiar avans del examen he fet com una "factura" que pots veure despres de comprar els productes, factura entre cometes ja que no pots imprimirla ni guardartela ja que al destruir la sesió, es pert la informació. 

En un principi que jo recorde acò es lo que he añadit de més.

Observacions:
	-Deuria cambiar la sentensia de la BD per a que funsionara del tot be, ja que em porta errors al hora de visualitzar el profile.
	-No tinc clau primaria a la tabla de like, no crec que done ningun error.
	
Crec que estos són tots els fallos del examen.