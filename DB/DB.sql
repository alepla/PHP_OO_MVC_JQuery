CREATE DATABASE Lawyers;
use Lawyers;
CREATE TABLE info(
    name varchar(60),
    lastname varchar(100),
    dni varchar(200) not null PRIMARY KEY, 
    tlp varchar(60),
    gender varchar(30),
    date0 varchar(60),
    date1 varchar(60),
    message text,
    studies varchar(60),
    Salary varchar(60),
    Hour time,
    Day date
);
CREATE TABLE dumies(
    name varchar(60),
    lastname varchar(100),
    image varchar(200),
    dni varchar(200) not null PRIMARY KEY, 
    tlp varchar(60),
    gender varchar(30),
    message text,
    date0 varchar(60),
    date1 varchar(60),
    type varchar(60),
    city varchar(60),
    studies varchar(60),
    id varchar(60),
    Salary varchar(60)
);
CREATE TABLE contact(
    name varchar(60),
    email varchar(100),
    message text,
    Hour time,
    Day date
);
CREATE TABLE review(
    name varchar(60),
    email varchar(100),
    message text,
    estrellas varchar(60),
    Hour time,
    Day date
);
CREATE TABLE login(
    name varchar(100),
    lastname varchar(100),
    username varchar(100) not null PRIMARY KEY,
    email varchar(100) not null UNIQUE,
    passwd varchar(100),
    type varchar(100)
);
CREATE TABLE shop(
    id int NOT NULL AUTO_INCREMENT,
    user varchar(100),
    prod varchar(100),
    price varchar(100),
    cant varchar(100),
    PRIMARY KEY(id)
);

INSERT INTO `info` (`name`, `lastname`, `dni`, `tlp`, `gender`, `date0`, `date1`, `message`, `studies`, `Salary`, `Hour`, `Day`) VALUES
('Harvey', 'Specter', '59264648E', '685631041', 'Male', '19/07/2012', '12/03/2020', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', 'Oxford', '1000', '12:08:26','2017-12-29'),
('Oscar', 'Joy', '87654321X', '685634101', 'Male' , '06/06/2011', '07/02/2030','Ms. Urquhart has been counsel in a number of high profile cases including securing.', 'Hardvard', '2000', '16:56:20','2017-12-29'),
('Alex', 'Pla', '12345678Z', '698675341','Male' , '16/05/2012', '09/09/2018','Ms. Urquhart has been counsel in a number of high profile cases including securing.', 'Columbia', '3000', '23:48:46','2017-12-29');

INSERT INTO `dumies` (`name`, `lastname`, `image`, `dni`, `tlp`, `gender`, `message`, `date0`, `date1`, `type`, `city`, `studies`, `id`, `Salary`) VALUES
('Luis', 'Litt', 'view/img/Columbia1.jpg', '59264688J', '685631041','Male' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Criminal', 'Spain', 'Columbia', '1', '1000'),
('Rachel', 'Ross', 'view/img/Columbia2.jpg', '69264688K', '685631041','Female' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Corporate', 'New York', 'Columbia', '2', '2000'),
('Harvey', 'Pla', 'view/img/Columbia3.jpg', '79264688O', '685631041','Male' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Corporate', 'Britain', 'Columbia', '3', '4000'),
('Victoria', 'Cambra', 'view/img/Columbia4.jpg', '89264688A', '685631041','Female' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Corporate', 'New York', 'Columbia', '2', '3000'),
('Alex', 'Specter', 'view/img/Columbia5.jpg', '49264688E', '685631041','Male' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Criminal', 'Italy', 'Columbia', '4', '4000'),
('Jake', 'Vañó', 'view/img/Hardvard1.jpg', '29264688J', '685631041','Male' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'Britain', 'Hardvard', '3', '1000'),
('Altea', 'Llin', 'view/img/Hardvard2.jpg', '39264688K', '685631041','Female' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'New York', 'Hardvard', '2', '2000'),
('Jose', 'Moreno', 'view/img/Hardvard3.jpg', '19264688O', '685631041','Male' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Criminal', 'Italy', 'Hardvard', '4', '1000'),
('Valeria', 'Cano', 'view/img/Hardvard4.jpg', '99264688A', '685631041','Female' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Criminal', 'Spain', 'Hardvard', '1', '4000'),
('Raul', 'Castelló', 'view/img/Hardvard5.jpg', '59264688H', '685631041','Male' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Criminal', 'Spain', 'Hardvard', '1', '3000'),
('Steve', 'Ruiz', 'view/img/Oxford1.jpg', '29264688S', '685631041','Male' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'New York', 'Oxford', '2', '1000'),
('Sara', 'Garcia', 'view/img/Oxford2.jpg', '39264688Q', '685631041','Female' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'Britain', 'Oxford', '3', '2000'),
('Daniel', 'Ortiz', 'view/img/Oxford3.jpg', '19264688F', '685631041','Male' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'Britain', 'Oxford', '3', '3000'),
('Clara', 'Briggs', 'view/img/Oxford4.jpg', '99264688V', '685631041','Female' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'Britain', 'Oxford', '3', '1000'),
('Carlos', 'Bauti', 'view/img/Oxford5.jpg', '59264688Z', '685631041','Male' ,'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'Britain', 'Oxford', '3', '4000');

INSERT INTO `review` (`name`, `email`, `message`, `estrellas`, `Hour`, `Day`) VALUES
('Harvey', 'harveyspecter@gmail.com', 'Check out our Streaming Guide to find out what shows are trending, if your favorite streaming show has been canceled, and what to binge next.', '★★★★', '12:08:26','2017-12-29'),
('Luis', 'luislitt@gmail.com', 'Heres a rundown of the 25 top-grossing films of the past year at the U.S. box office (as of Dec. 27, 2017). How many of these crowd-pleasers did you watch?', '★★', '16:56:20','2017-12-29'),
('Mike', 'mikeross@gmail.com', 'Which movies and TV shows piqued IMDb users interests the most in the past year? Here are our lists of the top titles added to IMDb Watchlists in 2017.
', '★★★', '23:48:46','2017-12-29');