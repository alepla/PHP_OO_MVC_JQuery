-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 12-03-2018 a las 10:51:04
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lawyers`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contact`
--

CREATE TABLE `contact` (
  `name` varchar(60) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `message` text,
  `Hour` time DEFAULT NULL,
  `Day` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contact`
--

INSERT INTO `contact` (`name`, `email`, `message`, `Hour`, `Day`) VALUES
('Alejandro Pla Cambra', 'aleplacambra@gmail.com', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '12:43:22', '2017-12-28'),
('Alejandro Pla Cambra', 'aleplacambra@gmail.com', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '12:44:40', '2017-12-28'),
('Alejandro Pla Cambra', 'aleplacambra@gmail.com', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '13:51:05', '2017-12-28'),
('Alejandro Pla Cambra', 'aleplacambra@gmail.com', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '15:44:52', '2017-12-28'),
('Jose', 'aleplacambra@gmail.com', 'ASDSADSADSADSADSADSADSA', '15:55:22', '2017-12-28'),
('Alejandro Pla Cambra', 'aleplacambra@gmail.com', 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', '15:56:35', '2017-12-28'),
('Alejandro Pla Cambra', 'aleplacambra@gmail.com', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '11:11:24', '2017-12-29'),
('Alejandro Pla Cambra', 'aleplacambra@gmail.com', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaassss', '20:48:56', '2018-01-02'),
('Alejandro Pla Cambra', 'aleplacambra@gmail.com', 'asdddddddddddddddddddddddddddddddddddddd', '11:54:50', '2018-01-03'),
('Alejandro Pla Cambra', 'aleplacambra@gmail.com', 'sadsadsadsadsadsadsadsadsadasddasd', '15:59:20', '2018-01-10'),
('Jose Moreno', 'aleplacambra@gmail.com', 'Hola dsadsadsadsadsadsadsa', '17:18:18', '2018-01-29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dumies`
--

CREATE TABLE `dumies` (
  `name` varchar(60) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `dni` varchar(200) NOT NULL,
  `tlp` varchar(60) DEFAULT NULL,
  `gender` varchar(30) DEFAULT NULL,
  `message` text,
  `date0` varchar(60) DEFAULT NULL,
  `date1` varchar(60) DEFAULT NULL,
  `type` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `studies` varchar(60) DEFAULT NULL,
  `id` varchar(60) DEFAULT NULL,
  `Salary` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dumies`
--

INSERT INTO `dumies` (`name`, `lastname`, `image`, `dni`, `tlp`, `gender`, `message`, `date0`, `date1`, `type`, `city`, `studies`, `id`, `Salary`) VALUES
('Daniel', 'Ortiz', 'view/img/Oxford3.jpg', '19264688F', '685631041', 'Male', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'Britain', 'Oxford', '3', '3000'),
('Jose', 'Moreno', 'view/img/Hardvard3.jpg', '19264688O', '685631041', 'Male', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Criminal', 'Italy', 'Hardvard', '4', '1000'),
('Jake', 'Vañó', 'view/img/Hardvard1.jpg', '29264688J', '685631041', 'Male', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'Britain', 'Hardvard', '3', '1000'),
('Steve', 'Ruiz', 'view/img/Oxford1.jpg', '29264688S', '685631041', 'Male', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'New York', 'Oxford', '2', '1000'),
('Altea', 'Llin', 'view/img/Hardvard2.jpg', '39264688K', '685631041', 'Female', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'New York', 'Hardvard', '2', '2000'),
('Sara', 'Garcia', 'view/img/Oxford2.jpg', '39264688Q', '685631041', 'Female', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'Britain', 'Oxford', '3', '2000'),
('Alex', 'Specter', 'view/img/Columbia5.jpg', '49264688E', '685631041', 'Male', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Criminal', 'Italy', 'Columbia', '4', '4000'),
('Raul', 'Castelló', 'view/img/Hardvard5.jpg', '59264688H', '685631041', 'Male', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Criminal', 'Spain', 'Hardvard', '1', '3000'),
('Luis', 'Litt', 'view/img/Columbia1.jpg', '59264688J', '685631041', 'Male', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Criminal', 'Spain', 'Columbia', '1', '1000'),
('Carlos', 'Bauti', 'view/img/Oxford5.jpg', '59264688Z', '685631041', 'Male', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'Britain', 'Oxford', '3', '4000'),
('Rachel', 'Ross', 'view/img/Columbia2.jpg', '69264688K', '685631041', 'Female', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Corporate', 'New York', 'Columbia', '2', '2000'),
('Harvey', 'Pla', 'view/img/Columbia3.jpg', '79264688O', '685631041', 'Male', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Corporate', 'Britain', 'Columbia', '3', '4000'),
('Victoria', 'Cambra', 'view/img/Columbia4.jpg', '89264688A', '685631041', 'Female', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Corporate', 'New York', 'Columbia', '2', '3000'),
('Valeria', 'Cano', 'view/img/Hardvard4.jpg', '99264688A', '685631041', 'Female', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Criminal', 'Spain', 'Hardvard', '1', '4000'),
('Clara', 'Briggs', 'view/img/Oxford4.jpg', '99264688V', '685631041', 'Female', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', '19/07/2012', '12/03/2020', 'Employment', 'Britain', 'Oxford', '3', '1000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `info`
--

CREATE TABLE `info` (
  `name` varchar(60) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `dni` varchar(200) NOT NULL,
  `tlp` varchar(60) DEFAULT NULL,
  `gender` varchar(30) DEFAULT NULL,
  `date0` varchar(60) DEFAULT NULL,
  `date1` varchar(60) DEFAULT NULL,
  `message` text,
  `studies` varchar(60) DEFAULT NULL,
  `Salary` varchar(60) DEFAULT NULL,
  `Hour` time DEFAULT NULL,
  `Day` date DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `info`
--

INSERT INTO `info` (`name`, `lastname`, `dni`, `tlp`, `gender`, `date0`, `date1`, `message`, `studies`, `Salary`, `Hour`, `Day`, `country`, `city`) VALUES
('Carlos', 'Ruis', '49264686Q', '685631041', 'Male', '01/18/2018', '01/18/2018', 'asdsadsadsadsadsadsadsadsa', 'Columbia ', '3000', '17:26:04', '2018-01-11', NULL, NULL),
('Dani', 'Urtis', '49264687G', '685631041', 'Female', '01/05/2018', '01/05/2018', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'Oxford ', '3000', '11:03:50', '2017-12-29', NULL, NULL),
('Mike', 'Ross', '49264687I', '685631041', 'Male', '01/16/2018', '01/16/2018', 'ASDSDSADSADSADSADSA', 'Hardvard ', '1000', '17:22:51', '2018-01-09', NULL, NULL),
('Jennifer', 'Lawrence', '49264687J', '685631041', 'Female', '01/15/2018', '01/15/2018', 'VJHHJJJGGGG', 'Oxford ', '1000', '18:01:53', '2018-01-08', NULL, NULL),
('Valeria', 'Cano', '49264687K', '685631041', 'Female', '01/16/2018', '01/16/2018', 'SADASDSADSADASDASD', 'Columbia ', '1000', '15:52:18', '2018-01-09', NULL, NULL),
('Robert', 'Zane', '49264687Q', '685631041', 'Male', '01/16/2018', '01/16/2018', 'AASDSADSADASDSADASD', 'Columbia ', '1000', '17:23:41', '2018-01-09', NULL, NULL),
('Luis', 'Litt', '49264687Y', '685631041', 'Male', '01/16/2018', '01/16/2018', 'DSADSADSADSADSADA', 'Other ', '1000', '17:23:10', '2018-01-09', NULL, NULL),
('Jose', 'Moreno', '49264688F', '685631041', 'Male', '12/17/2017', '12/17/2017', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'Columbia ', '1000', '11:06:06', '2017-12-29', NULL, NULL),
('Alejandro', 'Pla', '49264688P', '685631041', 'Male', '01/16/2018', '01/16/2018', 'DSFDSFDSFDSFDSFDSFS', 'Oxford ', '1000', '17:22:31', '2018-01-09', NULL, NULL),
('Alejandro', 'Cambra', '49264688S', '685631041', 'Male', '01/18/2018', '01/18/2018', 'SADSSADSADSADSADS', 'Columbia ', '1000', '18:50:15', '2018-01-11', NULL, NULL),
('Raul', 'Cambra', '52264688H', '685631041', 'Male', '01/16/2018', '01/16/2018', 'ADASDSADSADSDSADSAD', 'Hardvard ', '1000', '17:24:07', '2018-01-09', NULL, NULL),
('Harvey', 'Specter', '59264648E', '685631041', 'Male', '19/07/2012', '12/03/2020', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', 'Oxford ', '1000', '12:08:26', '2017-12-29', NULL, NULL),
('Oscar', 'Joy', '87654321X', '685634101', 'Male', '06/06/2011', '07/02/2030', 'Ms. Urquhart has been counsel in a number of high profile cases including securing.', 'Hardvard Oxford ', '2000', '16:56:20', '2017-12-29', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `like_prod`
--

CREATE TABLE `like_prod` (
  `user` varchar(60) DEFAULT NULL,
  `dni` varchar(100) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `lastname` varchar(60) DEFAULT NULL,
  `image` varchar(30) DEFAULT NULL,
  `studies` varchar(60) DEFAULT NULL,
  `Salary` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `like_prod`
--

INSERT INTO `like_prod` (`user`, `dni`, `name`, `lastname`, `image`, `studies`, `Salary`) VALUES
('Alepla', '19264688F', 'Daniel', 'Ortiz', 'view/img/Oxford3.jpg', 'Oxford', '3000'),
('Jose', '29264688J', 'Jake', 'Vañó', 'view/img/Hardvard1.jpg', 'Hardvard', '1000'),
('Alepla', '29264688S', 'Steve', 'Ruiz', 'view/img/Oxford1.jpg', 'Oxford', '1000'),
('Jose', '39264688K', 'Altea', 'Llin', 'view/img/Hardvard2.jpg', 'Hardvard', '2000'),
('Daniel', '49264688E', 'Alex', 'Specter', 'view/img/Columbia5.jpg', 'Columbia', '4000'),
('Daniel', '59264688H', 'Raul', 'Castelló', 'view/img/Hardvard5.jpg', 'Hardvard', '3000'),
('Cruiz', '79264688O', 'Harvey', 'Pla', 'view/img/Columbia3.jpg', 'Columbia', '4000'),
('Cruiz', '29264688S', 'Steve', 'Ruiz', 'view/img/Oxford1.jpg', 'Oxford', '1000'),
('Cruiz', '29264688J', 'Jake', 'Vañó', 'view/img/Hardvard1.jpg', 'Hardvard', '1000'),
('Cruiz', '29264688J', 'Jake', 'Vañó', 'view/img/Hardvard1.jpg', 'Hardvard', '1000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `name` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `passwd` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`name`, `lastname`, `username`, `email`, `passwd`, `type`) VALUES
('Alejandro', 'Pla', 'Alepla', 'aleplacambra@gmail.com', '$lMe8/Q8YJdNE', '1'),
('Carlos', 'Ruiz', 'Cruiz', 'carlos@gmail.com', '$lKYRRssW77A2', '2'),
('Dani', 'Ortiz', 'Daniel', 'dani@gmail.com', '$lJjTjLiqTLK6', '2'),
('Jose', 'Moreno', 'Jose', 'jose@gmil.com', '$l9NE9PTTL/mA', '2'),
('Raul', 'Cambra', 'Raul', 'raulcambra@gmail.com', '$l2YKOwjb9vrU', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `review`
--

CREATE TABLE `review` (
  `name` varchar(60) DEFAULT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `message` text,
  `estrellas` varchar(60) DEFAULT NULL,
  `Hour` time DEFAULT NULL,
  `Day` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `review`
--

INSERT INTO `review` (`name`, `email`, `message`, `estrellas`, `Hour`, `Day`) VALUES
('Kevin', 'alejandroplacambra@hotmail.com', 'Check out our Streaming Guide to find out what shows are trending, if your favorite streaming show has been canceled, and what to binge next.', '★', '19:43:24', '2017-12-28'),
('Alejandro Pla', 'aleplacambra@gmail.com', 'Heres a rundown of the 25 top-grossing films of the past year at the U.S. box office (as of Dec. 27, 2017). How many of these crowd-pleasers did you watch?', '★★', '19:35:11', '2017-12-28'),
('Javi Vañó', 'Crcalabuig18@hotmail.com', 'Which movies and TV shows piqued IMDb users interests the most in the past year? Here are our lists of the top titles added to IMDb Watchlists in 2017.\r\n', '★★★★★', '19:40:11', '2017-12-28'),
('Carlos Ruiz', 'Crcalabuig@gmail.com', 'Which of the 2018 Golden Globe Award nominees in the category for Best Limited Series or Motion Picture is your favorite? Discuss here after voting.\r\n', '★★★★', '19:33:36', '2017-12-28'),
('Harvey', 'harveyspecter@gmail.com', 'Check out our Streaming Guide to find out what shows are trending, if your favorite streaming show has been canceled, and what to binge next.', '★★★★', '12:08:26', '2017-12-29'),
('Luis', 'luislitt@gmail.com', 'Heres a rundown of the 25 top-grossing films of the past year at the U.S. box office (as of Dec. 27, 2017). How many of these crowd-pleasers did you watch?', '★★', '16:56:20', '2017-12-29'),
('Mike', 'mikeross@gmail.com', 'Which movies and TV shows piqued IMDb users interests the most in the past year? Here are our lists of the top titles added to IMDb Watchlists in 2017.\n', '★★★', '23:48:46', '2017-12-29'),
('Asdaasd', 'yomogan@gmail.com', 'A fdafsdfdsfsdfsdfdsfsd', '★★★★', '21:33:06', '2018-03-06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shop`
--

CREATE TABLE `shop` (
  `id` int(11) NOT NULL,
  `user` varchar(100) DEFAULT NULL,
  `prod` varchar(100) DEFAULT NULL,
  `price` varchar(100) DEFAULT NULL,
  `cant` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `shop`
--

INSERT INTO `shop` (`id`, `user`, `prod`, `price`, `cant`) VALUES
(170, 'Alepla', '29264688S', '3000', '3'),
(171, 'Alepla', '29264688S', '3000', '3'),
(172, 'Alepla', '29264688S', '3000', '3'),
(173, 'Alepla', '29264688J', '4000', '4'),
(210, 'Alepla', '29264688S', '1000', '1'),
(211, 'Alepla', '29264688S', '1000', '1'),
(212, 'Alepla', '79264688O', '4000', '1'),
(213, 'Alepla', '29264688S', '1000', '1'),
(214, 'Alepla', '29264688J', '1000', '1'),
(215, 'Alepla', '29264688S', '1000', '1'),
(216, 'Alepla', '29264688J', '1000', '1'),
(217, 'Alepla', '29264688S', '1000', '1'),
(218, 'Alepla', '29264688J', '1000', '1'),
(219, 'Alepla', '29264688S', '1000', '1'),
(220, 'Alepla', '29264688J', '1000', '1'),
(221, 'Alepla', '29264688S', '1000', '1'),
(222, 'Alepla', '29264688J', '1000', '1'),
(223, 'Alepla', '29264688S', '1000', '1'),
(224, 'Alepla', '29264688J', '1000', '1'),
(225, 'Alepla', '29264688S', '1000', '1'),
(226, 'Alepla', '29264688J', '1000', '1'),
(227, 'Alepla', '29264688S', '1000', '1'),
(228, 'Alepla', '29264688J', '1000', '1'),
(229, 'Alepla', '29264688S', '1000', '1'),
(230, 'Alepla', '29264688J', '1000', '1'),
(231, 'Alepla', '29264688S', '1000', '1'),
(232, 'Alepla', '29264688J', '1000', '1'),
(233, 'Alepla', '29264688S', '1000', '1'),
(234, 'Alepla', '29264688J', '1000', '1'),
(235, 'Alepla', '29264688S', '1000', '1'),
(236, 'Alepla', '29264688J', '1000', '1'),
(237, 'Alepla', '29264688S', '1000', '1'),
(238, 'Alepla', '29264688J', '1000', '1'),
(239, 'Alepla', '29264688S', '1000', '1'),
(240, 'Alepla', '29264688J', '1000', '1'),
(241, 'Alepla', '29264688S', '1000', '1'),
(242, 'Alepla', '29264688J', '1000', '1'),
(243, 'Alepla', '29264688S', '1000', '1'),
(244, 'Alepla', '29264688J', '1000', '1'),
(245, 'Alepla', '29264688S', '1000', '1'),
(246, 'Alepla', '29264688J', '1000', '1'),
(247, 'Alepla', '29264688S', '1000', '1'),
(248, 'Alepla', '29264688J', '1000', '1'),
(249, 'Alepla', '29264688S', '1000', '1'),
(250, 'Alepla', '29264688J', '1000', '1'),
(251, 'Alepla', '29264688S', '1000', '1'),
(252, 'Alepla', '29264688J', '1000', '1'),
(253, 'Alepla', '29264688S', '1000', '1'),
(254, 'Alepla', '29264688J', '1000', '1'),
(255, 'Alepla', '29264688S', '1000', '1'),
(256, 'Alepla', '29264688J', '1000', '1'),
(257, 'Alepla', '19264688O', '1000', '1'),
(258, 'Alepla', '29264688S', '1000', '1'),
(259, 'Alepla', '29264688J', '1000', '1'),
(260, 'Alepla', '19264688O', '1000', '1'),
(261, 'Alepla', '29264688S', '1000', '1'),
(262, 'Alepla', '29264688J', '1000', '1'),
(263, 'Alepla', '29264688S', '1000', '1'),
(264, 'Alepla', '29264688J', '1000', '1'),
(265, 'Alepla', '29264688S', '1000', '1'),
(266, 'Alepla', '29264688S', '1000', '1'),
(267, 'Alepla', '59264688H', '3000', '1'),
(268, 'Alepla', '29264688S', '1000', '1'),
(269, 'Alepla', '29264688S', '1000', '1'),
(270, 'Alepla', '59264688H', '3000', '1'),
(271, 'Alepla', '29264688S', '1000', '1'),
(272, 'Alepla', '29264688S', '1000', '1'),
(273, 'Alepla', '29264688S', '1000', '1'),
(274, 'Alepla', '29264688S', '1000', '1'),
(275, 'Alepla', '29264688S', '1000', '1'),
(276, 'Alepla', '29264688S', '1000', '1'),
(277, 'Alepla', '29264688S', '1000', '1'),
(278, 'Alepla', '29264688S', '1000', '1'),
(279, 'Alepla', '29264688S', '1000', '1'),
(280, 'Alepla', '29264688S', '1000', '1'),
(281, 'Alepla', '29264688S', '1000', '1'),
(282, 'Alepla', '29264688S', '1000', '1'),
(283, 'Alepla', '29264688S', '1000', '1'),
(284, 'Alepla', '29264688S', '1000', '1'),
(285, 'Alepla', '29264688S', '1000', '1'),
(286, 'Alepla', '29264688S', '1000', '1'),
(287, 'Alepla', '29264688S', '1000', '1'),
(288, 'Alepla', '29264688S', '1000', '1'),
(289, 'Alepla', '29264688S', '1000', '1'),
(290, 'Alepla', '29264688S', '1000', '1'),
(291, 'Alepla', '29264688S', '1000', '1'),
(292, 'Alepla', '29264688S', '1000', '1'),
(293, 'Alepla', '29264688S', '1000', '1'),
(294, 'Alepla', '29264688S', '1000', '1'),
(295, 'Alepla', '29264688S', '1000', '1'),
(296, 'Alepla', '29264688S', '1000', '1'),
(297, 'Alepla', '29264688S', '1000', '1'),
(298, 'Alepla', '29264688S', '1000', '1'),
(299, 'Alepla', '29264688S', '1000', '1'),
(300, 'Alepla', '29264688S', '1000', '1'),
(301, 'Alepla', '29264688S', '1000', '1'),
(302, 'Alepla', '29264688S', '1000', '1'),
(303, 'Alepla', '29264688S', '1000', '1'),
(304, 'Alepla', '29264688S', '1000', '1'),
(305, 'Alepla', '29264688S', '1000', '1'),
(306, 'Alepla', '29264688J', '1000', '1'),
(307, 'Alepla', '29264688S', '1000', '1'),
(308, 'Alepla', '29264688J', '1000', '1'),
(309, 'Alepla', '29264688J', '1000', '1'),
(310, 'Alepla', '29264688J', '1000', '1'),
(311, 'Alepla', '29264688J', '1000', '1'),
(312, 'Alepla', '29264688J', '1000', '1'),
(313, 'Alepla', '29264688S', '1000', '1'),
(350, 'Alepla', '19264688O', '3000', '3'),
(351, 'Alepla', '29264688J', '2000', '2'),
(352, 'Alepla', '29264688S', '1000', '1'),
(353, 'Alepla', '29264688J', '1000', '1'),
(354, 'Alepla', '29264688S', '1000', '1'),
(355, 'Alepla', '29264688S', '1000', '1'),
(356, 'Alepla', '29264688J', '1000', '1'),
(357, 'Cruiz', '29264688S', '1000', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `dumies`
--
ALTER TABLE `dumies`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`email`);

--
-- Indices de la tabla `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=358;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
