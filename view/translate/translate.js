frases = {
    "es": {
        "English": "Inglés",
        "Spanish": "Español",
        "Valencià" : "Valenciano",
        "Home" : "Inicio",
        "Lawyers": "Abogados",
        "Datatable": "Tabla",
        "Shop": "Tienda",
        "Meet us": "Conocenos",
        "Contact" : "Contactanos",
        "Search" : "Búscanos!",
        "Contact us" : "Contacto",
        "Reviews" : "Opioniones",
        "law" : "Nuestros abogados",
        "studies" : "Estudios",
        "form" : "Formulario de abogados",
        "Name" : "Nombre",
        "Last Name" : "Apellido",
        "Telephone" : "Teléfono",
        "Start Date" : "Fecha de inicio",
        "End Date" : "Fecha final",
        "Gender" : "Género",
        "Message" : "Mensaje",
        "Studies" : "Estudios ",
        "Salary:" : "Salario:",
        "Back" : "Volver",
        "Lawyers list" : "Lista de abogados",
        "create" : "Crear",
        "Name" : "<strong>Nombre</strong>",
        "Lastname" : "<strong>Apellido</strong>",
        "Actions" : "<strong>Acciones</strong>",
        "read" : "Leer",
        "update" : "Modificar",
        "delete" : "Eliminar",
        "Welcome" : "Bienvenidos",
        "Advantages" : "Ventajas",
        "Gallery" : "Galeria",
        "Call Us Now" : "LLamanos ahora",
        "Address" : "Dirección",
        "Send a message" : "Envianos un mensaje",
        "Hours Work" : "Horas de trabajo",
        "Subscribe Now" : "Suscribete ahora",
    },

    "vl": {
        "English": "Inglés",
        "Spanish": "Espanyol",
        "Valencià" : "Valencià",
        "Home" : "Inici",
        "Lawyers": "Adbocats",
        "Datatable": "Taulell",
        "Shop": "Tenda",
        "Meet us": "Coneixnos",
        "Contact" : "Contactans",
        "Reviews" : "Opinions",
        "Search" : "Búscans!",
        "Contact us" : "Contacte",
        "law" : "Els nostres adbocats",
        "Impartiality" : "Imparcialitat",
        "studies" : "Estudis",
        "Name" : "Nom",
        "Last Name" : "Cognom",
        "Telephone" : "Teléfon",
        "Start Date" : "Data de inici",
        "End Date" : "Data final",
        "Gender" : "Génere",
        "Message" : "Mensaje",
        "Studies" : "Estudis ",
        "Salary:" : "Salari:",
        "Back" : "Tornar",
        "Lawyers list" : "Llista de adbocats",
        "create" : "Crear",
        "Name" : "<strong>Nom</strong>",
        "Lastname" : "<strong>Cognom</strong>",
        "Actions" : "<strong>Accions</strong>",
        "read" : "Llegir",
        "update" : "Modificar",
        "delete" : "Eliminar",
        "Welcome" : "Benvinguts",
        "Advantages" : "Ventatjes",
        "Gallery" : "Galeria",
        "Call Us Now" : "Cridans ara",
        "Address" : "Adreça",
        "Send a message" : "Envians un missatge",
        "Hours Work" : "Hores de treball",
        "Subscribe Now" : "Suscribeixte ara"
    }
}
    /*function cambiarIdioma(lang) {
        var elems = document.querySelectorAll(".intfrase");
            for (var x = 0; x < elems.length; x++)
                elems[x].innerHTML = frases[lang][ elems[x].dataset.id ];
          }
    cambiarIdioma("en");*/

    function cambiarIdioma(lang) {
      lang = lang || localStorage.getItem('app-lang') || 'en';
      localStorage.setItem('app-lang', lang);

      var elems = document.querySelectorAll('[data-tr]');
      for (var x = 0; x < elems.length; x++) {
        elems[x].innerHTML = frases.hasOwnProperty(lang)
          ? frases[lang][elems[x].dataset.tr]
          : elems[x].dataset.tr;
      }
    }
    window.onload = function(){
      cambiarIdioma();
      
      document
        .getElementById('btn-es')
        .addEventListener('click', cambiarIdioma.bind(null, 'es'));

      document
        .getElementById('btn-en')
        .addEventListener('click', cambiarIdioma.bind(null, 'en'));

      document
        .getElementById('btn-vlc')
        .addEventListener('click', cambiarIdioma.bind(null, 'vl'));
    }