<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Pla's lawyers</title>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese" rel="stylesheet">  
    
    <link href="view/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="view/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="view/css/font-awesome.css" rel="stylesheet"> 

    <!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>

    <script>
        $( function() {
          var dateFormat = "mm/dd/yy",
            date0 = $( "#date0" )
              .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 2
              })
              .on( "change", function() {
                date1.datepicker( "option", "minDate", getDate( this ) );
              }),
            date1 = $( "#date1" ).datepicker({
              defaultDate: "+1w",
              changeMonth: true,
              numberOfMonths: 2
            })
            .on( "change", function() {
              date0.datepicker( "option", "maxDate", getDate( this ) );
            });
       
          function getDate( element ) {
            var date;
            try {
              date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
              date = null;
            }
       
            return date;
          }
        } );
    </script>

  	<script src="module/lawyer/model/validate_lawyer.js"></script>
    <script src="module/lawyer/model/modal_lawyer.js"></script>
    <script src="view/translate/translate.js"></script>
    <script src="module/review/model/validate_review.js"></script>
    <script src="module/shop/model/modal_shop.js"></script>
    <script src="module/shop/model/dropdown.js"></script>
    <script src="module/shop/model/filtrar_prod.js"></script>
    <script src="module/contact/model/validate_contact.js"></script>
    <script src="module/login/model/validate_login.js"></script>
    <script src="module/login/model/validate_register_login.js"></script>
    <script src="module/login/model/validate_recover_passwd.js"></script>
    <script src="module/shop/model/api.js"></script>
    <script src="module/contact/model/api.js"></script>
    <script src="module/cart/model/cart.js"></script>
    <script src="module/profile/model/profile.js"></script>

    <script src="view/js/bootstrap.js"></script>
    
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9sI_hdJ4VEOIe3RWnOa5Rzp5Y8SEHEK0&callback=initMap">
    </script>

</head>
<body>