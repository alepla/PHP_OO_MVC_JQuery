<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1 minimum-scale=1" />
    <title>Pla's lawyers</title>

    <link href="view/css/font-awesome.css" rel="stylesheet"> 
    <link href="view/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="view/css/style.css" rel="stylesheet" type="text/css" media="all" />

    <link rel="stylesheet" href="module/Filtrar_Paginar/view/lib/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />

    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese" rel="stylesheet">

    <script type="text/javascript" src="module/Filtrar_Paginar/view/lib/jqwidgets/scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="module/Filtrar_Paginar/view/lib/jqwidgets/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="module/Filtrar_Paginar/view/lib/jqwidgets/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="module/Filtrar_Paginar/view/lib/jqwidgets/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="module/Filtrar_Paginar/view/lib/jqwidgets/jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="module/Filtrar_Paginar/view/lib/jqwidgets/jqwidgets/jqxdatatable.js"></script> 
    <script type="text/javascript" src="module/Filtrar_Paginar/view/lib/jqwidgets/scripts/demos.js"></script>

    <script type="text/javascript" src="module/Filtrar_Paginar/model/datatable.js"></script>

    <script src="view/js/bootstrap.js"></script>

    <script src="view/translate/translate.js"></script>
    
</head>
<body class='default'>