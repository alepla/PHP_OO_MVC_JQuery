<div id="footer">
	<div class="footer">
		<div class="container">
			<div class="agileits_w3layouts_footer_grids">
				<div class="col-md-4 agileits_w3layouts_footer_grid">
					<h3 data-tr="Call Us Now">Call Us Now</h3>
					<ul>
						<li><span>Office Phone :</span> (+123) 2302 232</li>
						<li><span>Fax :</span> (+123) 123 456 455</li>
					</ul>
				</div>
				<div class="col-md-4 agileits_w3layouts_footer_grid">
					<h3 data-tr="Address">Address</h3>
					<p> 132 New York, 230th Street <i>4th United States.</i></p>
				</div>
				<div class="col-md-4 agileits_w3layouts_footer_grid">
					<h3 data-tr="Send a message">send a message</h3>
					<ul>
						<li><span>Email :</span> <a href="mailto:plalaw@gmail.com">plalaw@gmail.com</a></li>
						<li><span>Enquiry :</span> <a href="mailto:plalaw@hotmail.com">plalaw@hotmail.com</a></li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="w3_agileits_footer_grids">
				<div class="col-md-4 w3_agileits_footer_grid">
					<p>I help nonprofits and arts organizations solve problems
					 so they can stop worrying and focus on what matters most – The Work.</p>
				</div>
				<div class="col-md-4 w3_agileits_footer_grid">
					<h3 data-tr="Hours Work">Hours Work</h3>
					<h4>Offices are opened</h4>
					<ul>
						<li><span>Mon - Fri : </span> 9:00 AM to 6:00PM</li>
						<li><span>Sat : </span> 10:00 AM to 3:00PM</li>
					</ul>
				</div>
				<div class="col-md-4 w3_agileits_footer_grid">
					<h3 data-tr="Subscribe Now">Subscribe Now</h3>
					<form action="#" method="post">
						<input type="email" name="Email" placeholder="Email" required="">
						<input type="submit" value="Submit ">
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<div class="wthree_copy_right copyright">
		<div class="container">
			<div class="copyrighttop">
				<ul>
					<li><a class="facebook" href="https://es-es.facebook.com/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a class="facebook" href="https://twitter.com/"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a class="facebook" href="https://dribbble.com/"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
					<li><a class="facebook" href="https://www.google.es/"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				</ul>
			</div>
			<p>© 2018 Hauling. All rights reserved | Design by <a href="http://w3layouts.com/">Pla's Lawyers</a></p>
		</div>
	</div>
</div>