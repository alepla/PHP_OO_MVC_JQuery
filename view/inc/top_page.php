<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Pla's Lawyers</title>
					
	<link rel="stylesheet" href="view/css/flexslider.css" type="text/css" media="all" />

	<link href="view/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="view/css/style.css" rel="stylesheet" type="text/css" media="all" />

	<link href="view/css/font-awesome.css" rel="stylesheet"> 

	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese" rel="stylesheet">

	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>

	<script type="text/javascript" src="view/js/jquery-2.1.4.min.js"></script>
	<script src="view/js/bootstrap.js"></script>

	<script defer src="view/js/jquery.flexslider.js"></script>
	<script type="text/javascript">
		$(function(){
			SyntaxHighlighter.all();
		});
		$(window).load(function(){
			$('.flexslider').flexslider({
				animation: "slide",
				start: function(slider){
					$('body').removeClass('loading');
				}
			});
		});
	</script>

	<script src="view/js/jquery.waypoints.min.js"></script>
	<script src="view/js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
	<script src="view/translate/translate.js"></script>

</head>
<body>