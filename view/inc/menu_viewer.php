<div id="menu">
	<div class="banner">
		<div class="banner-dott">
			<div class="top-bar">
				<div class="container">
					<div class="header">
						<nav class="navbar navbar-default">
							<div class="navbar-header navbar-left">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<h1><a class="navbar-brand" href="index.php?page=homepage">Pla's Lawyers</span></a></h1>
							</div>
							<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
								<nav class="cl-effect-15" id="cl-effect-15">
									<ul class="nav navbar-nav">
										<li><a data-tr="Home" class="active" href="index.php?page=homepage">Home</a></li>
										<li><a data-tr="Shop" href="index.php?page=controller_shop&op=dumies">Shop</a></li>
										<li><a data-tr="Reviews" href="index.php?page=controller_review&op=list_reviews">Reviews</a></li>
										<li><a data-tr="Meet us" href="index.php?page=aboutus">Meet us</a></li>
										<li><a data-tr="Contact us" href="index.php?page=controller_contact&op=contact">Contact us</a></li>
										<li><a href="index.php?page=controller_login&op=list_login">Login</a></li>
										<li><a href="index.php?page=controller_cart&op=cart"><i class="glyphicon glyphicon-shopping-cart carrito"></i><div id="num_prod"></div></a></li>
									</ul>
								</nav>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>