<?php
	if(!$_GET){
		$_GET['page']="homepage";
		$_GET['op']="list";
	}
	switch($_GET['page']){
		case "homepage":
			include("module/inicio/view/inicio.php");
			break;
		case "controller_lawyer":
			include("module/lawyer/controller/".$_GET['page'].".php");
			break;
		case "controller_datatable":
			include("module/Filtrar_Paginar/controller/".$_GET['page'].".php");
			break;
		case "controller_shop":
			include("module/shop/controller/".$_GET['page'].".php");
			break;
		case "aboutus":
			include("module/aboutus/".$_GET['page'].".php");
			break;
		case "controller_review":
			include("module/review/controller/".$_GET['page'].".php");
			break;
		case "controller_contact":
			include("module/contact/controller/".$_GET['page'].".php");
			break;
		case "controller_login":
			include("module/login/controller/".$_GET['page'].".php");
			break;
		case "controller_profile":
			include("module/profile/controller/".$_GET['page'].".php");
			break;
		case "controller_cart":
			include("module/cart/controller/".$_GET['page'].".php");
			break;
		case "404":
			include("view/inc/error".$_GET['page'].".php");
			break;
		case "503":
			include("view/inc/error".$_GET['page'].".php");
			break;
		default:
			include("module/inicio/view/inicio.php");
			break;
	}
?>