<div id="header">
	<div class="banner">
		<div class="banner-dott">
			<div class="top">
				<div class="container">
					<div class="col-md-6 top-left">
						<ul>
							<li><i class="fa fa-mobile" aria-hidden="true"></i> +685 631 152</li>
							<li><i class="fa fa-map-marker" aria-hidden="true"></i> 132 New York, 230th Street </li>
						</ul>
					</div>
					<div class="col-md-6 top-middle">
						<ul>
							<li><a href="https://es-es.facebook.com/"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
							<li><a href="https://www.google.es/"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="https://es.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
					<div class="clearfix"></div>	
				</div>
			</div>
			<div class="btn-idiom">
				<button data-tr="Valencià" id="btn-vlc" type="button">Valencià</button>
				<button data-tr="Spanish" id="btn-es" type="button">Español</button>
				<button data-tr="English" id="btn-en" type="button">English</button>
			</div>
			<div class="w3-banner">
				<div id="typer"></div>
					<p>When you are backed against the wall, break the goddamn thing down.</p>
			</div>
		</div>
	</div>
</div>
