<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

    if ((isset($_GET['page'])) && ($_GET['page']==="controller_lawyer")){
		include("view/inc/top_page_lawyer.php");
	}elseif((isset($_GET['page'])) && ($_GET['page']==="controller_shop")){
        include("view/inc/top_page_lawyer.php");
    }elseif((isset($_GET['page'])) && ($_GET['page']==="controller_contact")){
        include("view/inc/top_page_lawyer.php");
    }elseif((isset($_GET['page'])) && ($_GET['page']==="controller_review")){
        include("view/inc/top_page_lawyer.php");
    }elseif((isset($_GET['page'])) && ($_GET['page']==="controller_datatable")){
        include("view/inc/top_page_datatable.php");
    }elseif((isset($_GET['page'])) && ($_GET['page']==="controller_login")){
        include("view/inc/top_page_lawyer.php");
    }elseif((isset($_GET['page'])) && ($_GET['page']==="controller_cart")){
        include("view/inc/top_page_lawyer.php");
    }elseif((isset($_GET['page'])) && ($_GET['page']==="controller_profile")){
        include("view/inc/top_page_lawyer.php");
    }else{
		include("view/inc/top_page.php");
	}
	session_start();
    @$type = $_SESSION['type'];
    //echo($type);
?>
<div id="wrapper">
    <div id="menu">
        <?php
            if($type == '1'){
                include("view/inc/menu_admin.php");
            }elseif($type == '2'){
                include("view/inc/menu_client.php");
            }else{
                include("view/inc/menu_viewer.php");
            }
        ?>
    </div>	
    <div id="header">    	
    	<?php
    	    include("view/inc/header.php");
    	?>        
    </div>  	
    <div id="">
    	<?php 
		    include("view/inc/pages.php"); 
		?>        
        <br style="clear:both;" />
    </div>
    <div id="footer">   	   
	    <?php
	        include("view/inc/footer.php");
	    ?>        
    </div>
</div>
<?php
    include("view/inc/bottom_page.php");
?>
    